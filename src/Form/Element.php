<?php

namespace Sulfur\Form;

class Element
{
	/**
	 * The key of the element
	 * @var string
	 */
	protected $key = null;

	/**
	 * Element type (select / text / etc.)
	 * @var string
	 */
	protected $type = null;

	/**
	 * Additional parameters
	 * @var array
	 */
	protected $params = [];

	/**
	 * Reference to the parent form
	 * @var \Sulfur\Form
	 */
	protected $form = null;

	/**
	 * for multiple elements with the same name: the index
	 * @var int
	 */
	protected $index = 0;


	/**
	 * Create an element
	 * @param string $key
	 * @param string $type
	 * @param array $params
	 * @param \Sulfur\Form\ $form
	 */
	public function __construct($key, $type, array $params, Form $form)
	{
		$this->key = $key;
		$this->type = $type;
		$this->params = $params;
		$this->form = $form;
	}


	/**
	 * Set index for multiple elements with the same name
	 * @param int $index
	 */
	public function index($index)
	{
		$this->index = $index;
	}


	/**
	 * Get value or a sub-value from a keyed element
	 * @param string $key
	 * @param mixed $default
	 * @return mixed
	 */
	public function value($key = null)
	{
		if($key === null) {
			return $this->value;
		} elseif(is_array($this->value) && isset($this->value[$key])){
			return $this->value[$key];
		} elseif(is_object($this->value) && isset($this->value->{$key})){
			return $this->value->{$key};
		}
	}


	/**
	 * Get name or a sub-keyed name
	 * @param string $key
	 * @return string
	 */
	public function name($key = null)
	{
		if($key === null) {
			return $this->name;
		} else {
			return $this->name . '['.$key.']';
		}
	}


	/**
	 * Magic get
	 * @param string $name
	 * @return string|null
	 */
	public function __get($name)
	{
		switch($name){
			case 'form':
				// get the form
				return $this->form;
			case 'name':
				// check with the form if this is a multiple or a set
				if($this->form->multiple($this->key) || $this->form->set($this->key)){
					return $this->key . '[]';
				} else {
					return $this->key;
				}
			case 'key':
			case 'type':
			case 'params':
				// just return the value
				return $this->{$name};
			case 'required':
				if(isset($this->params['required'])) {
					// if required was explicitely set in the params, return it
					return $this->params['required'];
				} else {
					// if not, check with the form if this is required
					// but only return true for the first element in a set
					return $this->index === 0 &&  $this->form->required($this->key);
				}
			case 'value':
				$value = $this->form->value($this->key);
				if(is_array($value) && $this->form->set($this->key) && isset($value[$this->index])){
					// get the nth value in a set
					return $value[$this->index];
				} elseif(is_array($value) && $this->form->set($this->key)) {
					// it is a set, but index not set
					return '';
				} else {
					return $value;
				}
			case 'error':
				$errors = $this->form->errors($this->key);
				// get first error, but only if element is the first in a set
				if(is_array($errors) && isset($errors[0]) && $this->index === 0){
					return $errors[0];
				} else {
					return null;
				}
			case 'errors':
				$errors = $this->form->errors($this->key);
				// get errors, but only if element is the first in a set
				if($this->index === 0){
					return $errors;
				} else {
					return null;
				}
			default:
				// return a user defined param
				if(isset($this->params[$name])){
					return $this->params[$name];
				} else {
					return null;
				}
		}
	}


	/**
	 * Magic set
	 * @param string $name
	 * @return Element
	 */
	public function __set($name, $value)
	{
		$this->params[$name] = $value;
		return $this;
	}


	/**
	 * Magic isset
	 * @param string $name
	 * @return bool
	 */
	public function __isset($name)
	{
		return in_array($name, ['name', 'key', 'type', 'params', 'required', 'value', 'error', 'errors'])
		|| isset($this->params[$name]);
	}
}