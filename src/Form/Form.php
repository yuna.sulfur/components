<?php

namespace Sulfur\Form;

use Closure;

use Sulfur\Form\Builder as Builder;
use Sulfur\Form\Validation as Validation;
use Sulfur\Form\Request as Request;

class Form
{
	/**
	 * Form id for multiple forms
	 * @var string
	 */
	protected $id = null;

	/**
	 * Validation object
	 * @var \Sulfur\Form\Validation
	 */
	protected $valdiation;

	/**
	 * Request object
	 * @var \Sulfur\Form\Request
	 */
	protected $request;


	/**
	 * Was the form processed
	 * @var boolean
	 */
	protected $processed = false;

	/**
	 * Was the form submitted
	 * @var boolean
	 */
	protected $submitted = false;

	/**
	 * Is the form valid
	 * @var boolean
	 */
	protected $valid = false;

	/**
	 * Errors after validating
	 * @var array
	 */
	protected $errors = [];

	/**
	 * form attributes
	 * @var array
	 */
	protected $attributes = [
		'action' => '',
		'method' => 'POST',
		'enctype' => 'multipart/form-data',
	];

	/**
	 * Form fields, holds elements, default values, etc.
	 * @var array
	 */
	protected $fields = [];

	/**
	 * List of elements.
	 * @var array
	 */
	protected $elements = [];

	/**
	 * The layout of the elements
	 * @var array
	 */
	protected $layout = [];

	/**
	 * Registered processors
	 * @var array
	 */
	protected $processors = [];

	/**
	 * Values by key
	 * @var array
	 */
	protected $values = null;


	/**
	 * Create a new form
	 * @param \Sulfur\Form\Builder $builder
	 */
	public function __construct(Builder $builder = null)
	{
		// create objects
		$this->validation = new Validation();
		$this->request = new Request();

		// build the form
		if($builder) {
			$builder->build($this);
		}
	}


	/**
	 * Get or set attribute
	 * @param string $name
	 * @param string $value
	 * @return string|\Sulfur\Form
	 */
	public function attribute($name, $value = null)
	{
		if($value === null){
			if(isset($this->attributes[$name])){
				return $this->attributes[$name];
			}
		} else {
			$this->attributes[$name] = $value;
			return $this;
		}
	}


	/**
	 * Add or get an element
	 * @param string $key
	 * @param string|int $typeOrIndex
	 * @param array $params
	 * @return null|array|\Sulfur\Form\Element
	 */
	public function element($key, $typeOrIndex = null, array $params = [])
	{
		if ($typeOrIndex === null) {
			// get element without index
			if (isset($this->fields[$key])) {
				if ($this->fields[$key]['set'] === true) {
					// get entire group
					return $this->fields[$key]['elements'];
				} else {
					// get first element
					return $this->fields[$key]['elements'][0];
				}
			} else {
				return null;
			}
		} elseif (is_int($typeOrIndex)) {
			// get indexed element
			if (isset($this->fields[$key]) && isset($this->fields[$key]['elements'][$typeOrIndex])) {
				return $this->fields[$key]['elements'][$typeOrIndex];
			} else {
				return null;
			}
		} else {
			// create element
			$type = $typeOrIndex;
			$element = new Element($key, $type, $params, $this);
			// store it
			$this->elements[] = $element;
			if (isset($this->fields[$key])) {
				// Element with the same key was already present
				// Make it a set of elements
				// Set the index in the element
				$this->fields[$key]['elements'][] = $element;
				$this->fields[$key]['set'] = true;
				$element->index(count($this->fields[$key]['elements']));
			} else {
				// create new field
				$this->fields[$key] = [
					'multiple' => isset($params['multiple']) ? $params['multiple'] : ($type === 'checkbox'),
					'set' => false,
					'keys' => isset($params['keys']) ? $params['keys'] : false,
					'elements' => [$element],
					'default' => isset($params['default']) ? $params['default'] : null,
					'required' => false,
				];
			}
			return $element;
		}
	}


	/**
	 * Add a rule
	 * @param string $key
	 * @param string $rule
	 */
	public function rule($key, $rule, ...$params)
	{
		if ($rule === 'required') {
			if (isset($this->fields[$key])) {
				$this->fields[$key]['required'] = true;
			}
		}
		$this->validation->rule($key, $rule, ...$params);
	}


	/**
	 * Add a processor
	 * @param string $key
	 * @param \Closure $processor
	 */
	public function processor($key, Closure $processor)
	{
		if(!isset($this->processors[$key])){
			$this->processors[$key] = [];
		}
		$this->processors[$key][] = $processor;
	}


	/**
	 * Overrule the $_GET or $_POST data with custom data
	 * @param array $data
	 */
	public function request($data = [])
	{
		$this->request->data($data);
		return $this;
	}


	/**
	 * Check if the form was submitted
	 * @return boolean
	 */
	public function submitted()
	{
		$this->process();
		return $this->submitted;
	}


	/**
	 * Check if the validation passed for one or all elements
	 * @return boolean
	 */
	public function valid($key = null)
	{
		$this->process();

		if($key === null){
			return $this->valid;
		} else {
			if (isset($this->errors[$key])) {
				return empty($this->errors[$key]);
			}
			return true;
		}
	}


	/**
	 * Get elements
	 * @return array
	 */
	public function elements()
	{
		$this->id();
		return $this->elements;
	}


	/**
	 * Get or set layout (used by Builder)
	 */
	public function layout($layout = null)
	{
		if($layout === null){
			$this->id();
			return $this->layout;
		} else {
			$this->layout = $layout;
		}
	}


	/**
	 * Tag form with hidden id field
	 */
	public function id($id = null)
	{
		if($this->id === null) {
			if($id) {
				$this->id = '__' . $id;
			} elseif(isset($this->attributes['id'])) {
				$this->id = '__' . $this->this->attributes['id'];
			} else {
				$this->id = '__' . substr(md5(implode(',', array_keys($this->fields))), 0 , 6);
			}

			// put hidden field with form id to track if form was submitted
			// and add hidden to layout
			array_unshift($this->layout, (object) [
				'type' => 'element',
				'element' => $this->element($this->id, 'hidden')
			]);
		}
		return $this->id;
	}


	/**
	 * Get the errors for a key or all errors
	 * @param string|null $key
	 * @return array
	 */
	public function errors($key = null)
	{
		$this->process();

		if ($key === null) {
			return $this->errors;
		} elseif (isset($this->errors[$key])) {
			return $this->errors[$key];
		} else {
			return [];
		}
	}


	/**
	 * Check if an element can contain multiple values
	 * @param string $key
	 * @return boolean
	 */
	public function multiple($key)
	{
		if (isset($this->fields[$key])) {
			return $this->fields[$key]['multiple'];
		} else {
			return false;
		}
	}


	/**
	 * Check if element is part of a set
	 * @param string $key
	 * @return boolean
	 */
	public function set($key)
	{
		if (isset($this->fields[$key])) {
			return $this->fields[$key]['set'];
		} else {
			return false;
		}
	}


	/**
	 * Check if element is required
	 * @param string $key
	 * @return boolean
	 */
	public function required($key)
	{
		if (isset($this->fields[$key])) {
			return $this->fields[$key]['required'];
		} else {
			return false;
		}
	}



	/**
	 * Get or set the values
	 * @param array|null $values
	 * @param boolean $complete Use defaults or '' to fill up non-supplied values
	 * @return array|void
	 */
	public function values($values = null)
	{
		if ($values === null) {
			// get values
			$this->process();
			// return values to fill up
			$values = [];
			foreach ($this->fields as $key => $field) {
				if (is_array($field['keys'])) {
					// flatten subkeys as main keys
					foreach ($field['keys'] as $subkey) {
						$values[$subkey] = $this->values[$key][$subkey];
					}
				} else {
					$values[$key] = $this->values[$key];
				}
			}
			// remove the automatically set id field
			unset($values[$this->id]);
			// return result
			return $values;
		} else {
			// first time calling values: start as empty array
			if($this->values === null){
				$this->values = [];
			}
			foreach ($this->fields as $key => $field) {
				if(is_array($field['keys'])){
					// create a compound element from provied and default values
					$value = [];
					foreach ($field['keys'] as $subkey) {
						if (isset($values[$subkey])) {
							// subkey was gicen as main key
							$value[$subkey] = $values[$subkey];
						} elseif(isset($values[$key]) && isset($values[$key][$subkey])) {
							// subkey was given as subkey of main key
							$value[$subkey] = $values[$key][$subkey];
						} elseif (is_array($field['default']) && isset($field['default'][$subkey])) {
							// default value given
							$value[$subkey] = $field['default'][$subkey];
						} else {
							// just empty string
							$value[$subkey] = '';
						}
					}
					$this->value($key, $value);
				} elseif (isset($values[$key])) {
					// plain value, is available, just set it
					$this->value($key, $values[$key]);
				} elseif ($field['default'] !== null) {
					// get provided default value
					$this->value($key, $field['default']);
				} elseif ($field['multiple']) {
					// set empty array
					$this->value($key, []);
				} else {
					// set empty string
					$this->value($key, '');
				}
			}

			// run processors after all values are set
			foreach ($this->fields as $key => $field) {
				if(isset($this->processors[$key]) && is_array($this->processors[$key])){
					foreach($this->processors[$key] as $processor){
						if(is_object($processor) && method_exists($processor, '__invoke')){
							$this->values[$key] = $processor->__invoke($this->values[$key]);
						}
					}
				}
			}

			return $this;
		}
	}


	/**
	 * Get or set a value
	 * @param string $key
	 * @param null|string|int|array $value
	 * @return string|array|void
	 */
	public function value($key, $value = null)
	{
		if ($value === null) {
			$this->process();
			if (isset($this->values[$key])) {
				// just return the value
				return $this->values[$key];
			} else {
				// try to find a subkey
				foreach ($this->fields as $superkey => $field) {
					if (is_array($field['keys']) && in_array($key, $field['keys'])) {
						return $this->values[$superkey][$key];
					}
				}
				// didnt work out
				return null;
			}
		} else {
			// set value
			if (isset($this->fields[$key])) {
				if (($this->fields[$key]['multiple'] || $this->fields[$key]['set']) && !is_array($value)) {
					$this->values[$key] = [$value];
				} else {
					$this->values[$key] = $value;
				}
			} else {
				// user is setting a specific subkey. shouldnt happen very often
				foreach ($this->fields as $superkey => $field) {
					if (is_array($field['keys']) && in_array($key, $field['keys'])) {
						$this->values[$superkey][$key] = $value;
					}
				}
			}
			return $this;
		}
	}


	/**
	 * Validate the form
	 */
	protected function validate()
	{
		$this->process();
		$this->valid = $this->validation->validate($this->values);
		$this->errors = $this->validation->errors();
	}


	/**
	 * Process the form
	 */
	protected function process()
	{
		if ($this->processed === false) {
			// process only once
			$this->processed = true;
			// get id
			$id = $this->id();
			// check if submitted
			if (strtoupper($this->attributes['method']) === 'POST') {
				$this->submitted = key_exists($id, $this->request->post());
			} else {
				$this->submitted = key_exists($id, $this->request->get());
			}

			if ($this->submitted) {
				if (strtoupper($this->attributes['method']) === 'POST') {
					$this->values($this->request->post());
				} else {
					$this->values($this->request->get());
				}
				$this->validate();
			} elseif($this->values === null) {
				// when no values were set manually and form not submitted
				// set empty values now, so default values will be used
				$this->values( [] );
			}
		}
	}
}
