<?php

namespace Sulfur\Form;

class Request
{
	protected $data = null;

	/**
	 * Manually set data used for get or post data
	 * @param array $data
	 */
	public function data(array $data = [])
	{
		$this->data = $data;
	}


	/**
	 * Get a GET value
	 * @param string $name
	 * @return mixed
	 */
	public function get($name = null)
	{
		$data = is_array($this->data) ? $this->data : $_GET;

		if($name === null){
			return $data;
		} else {
			if(isset($data[$name])){
				return $data[$name];
			}
		}
	}

	
	/**
	 * Get a POST value
	 * @param string $name
	 * @return mixed
	 */
	public function post($name = null)
	{
		$data = is_array($this->data) ? $this->data : $_POST;

		if($name === null){
			return $data;
		} else {
			if(isset($data[$name])){
				return $data[$name];
			}
		}
	}
}