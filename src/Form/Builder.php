<?php

namespace Sulfur\Form;

use Exception;
use Sulfur\Form\Form;

class Builder
{
	/**
	 * Dependencies added through constructor
	 * Implementation can use these through ::__get
	 * @var array
	 */
	protected $dependencies = [];


	/**
	 * Create a builder with extra dependencies
	 * @param array $dependencies
	 */
	public function __construct($dependencies = [])
	{
		$this->dependencies = $dependencies;
		$form = new Form($this);
	}

	/**
	 * Get a value from the provided dependencies
	 * @param string $name
	 * @return mixed
	 */
	public function __get($name)
	{
		if(isset($this->dependencies[$name])){
			return $this->dependencies[$name];
		}
	}


	/**
	 * Build the form based on the provided builder
	 * @param \Sulfur\Form $form
	 */
	public function build(Form $form)
	{
		// Build attributes
		$this->buildAttributes(method_exists($this, 'attributes') ? $this->attributes($form) : false, $form);

		// Build & get elements
		$elements = $this->buildElements(method_exists($this, 'elements') ? $this->elements($form) : false, $form);

		// Build rules
		$this->buildRules(method_exists($this, 'rules') ? $this->rules($form) : false, $form);

		// Build processors
		$this->buildProcessors(method_exists($this, 'processors') ? $this->processors($form) : false, $form);

		// Build layout
		$layout = $this->buildLayout(method_exists($this, 'layout') ? $this->layout($form) : false, $elements);

		// pass it to the form
		$form->layout($layout);
	}


	/**
	 * Set attributes on the form
	 * @param array $settings
	 * @param \Sulfur\Form $form
	 * @return void
	 */
	protected function buildAttributes($settings, Form $form)
	{
		if(is_array($settings)) {
			foreach($settings as $name => $value){
				$form->attribute($name, $value);
			}
		}
	}


	/**
	 * Set elements on the form
	 * @param array $settings
	 * @param \Sulfur\Form $form
	 * @return array
	 */
	protected function buildElements($settings, Form $form)
	{
		$elements = [];
		if(is_array($settings)) {
			foreach($settings as $params){
				if(!$params) {
					continue;
				}
				if(!is_array($params) || count($params) < 2){
					throw new Exception('Form element should be an array with at least two items (key, type)');
				}
				if(isset($params[0])){
					$key = array_shift($params);
				} else {
					throw new Exception('Trying to add element without valid key. Add it as the [0] item of an element');
				}
				if(isset($params[0])){
					$type = array_shift($params);
				} else {
					throw new Exception('Trying to add element without valid type. Add it as the [1] item of an element');
				}
				$elements[$key] = $form->element($key, $type, $params);
			}
		}
		return $elements;
	}


	/**
	 * Set rules on the form
	 * @param array $settings
	 * @param \Sulfur\Form $form
	 * @return void
	 */
	protected function buildRules($settings, Form $form)
	{
		if(is_array($settings)) {
			foreach($settings as $rule){
				if(! $rule) {
					continue;
				}

				if(! is_array($rule) || count($rule) < 2 ){
					throw new Exception('Rule should be of the form [key, rule, [arg, [arg ...]]]');
				}
				$form->rule(...$rule);
			}
		}
	}


	/**
	 * Set processors on the form
	 * @param array $settings
	 * @param \Sulfur\Form $form
	 * @return void
	 */
	protected function buildProcessors($settings, Form $form)
	{
		if(is_array($settings)) {
			foreach($settings as $key => $processors){
				if(! $processors) {
					continue;
				}
				if(is_object($processors) && method_exists($processors, '__invoke')) {
					// single processor given
					$form->processor($key, $processors);
				} elseif(is_array($processors)){
					// multiple processors given
					foreach($processors as $processor){
						$form->processor($key, $processor);
					}
				} else {
					throw new Exception('Processor(s) should be a closure or an array of closures');
				}
			}
		}
	}


	/**
	 * Set layout in the form
	 *
	 * Layout can have the following form
	 *
	 * [
	 *	'<elmentname>', // parsed layout: ['type' => 'element', 'element' => <Sulfur\Form\Element>]
	 *  '<markuptype>', // parsed layout: ['type' => 'markup', 'markup' => (object) ['type' => '<markuptype>']]
	 *  ['<markuptype>', '<param1>' => '<val1>', ...], // parsed layout: ['type' => 'markup', 'markup' => (object) ['type' => '<markuptype>', '<param1>' => '<val1>', ... ]]
	 *	['<grouptype>', [<nestedLayout>]] // parsed layout: ['type' => 'group', 'group' => ['type' => <grouptype>, 'elements' => [<parsedNestedLayout>]]]
	 *	['<grouptype>', [<nestedLayout>], '<param1>' => '<val1>', ...] // parsed layout: ['type' => 'group', 'group' => ['type' => <grouptype>, 'elements' => [<parsedNestedLayout>, '<param1>' => '<val1>', ... ]]]
	 * ]
	 *
	 * @param array $settings
	 * @param array $elements
	 * @return array
	 * @throws Exception
	 */
	protected function buildLayout($settings, $elements)
	{
		if(! is_array($settings)) {
			// no layout defined: use all the elements
			$settings = array_keys($elements);
		}

		$layout = [];

		foreach($settings as $params){
			if(! $params) {
				continue;
			}
			if(is_string($params)){
				// $params is a string
				// it can mean a reference to an element, or markup
				if(isset($elements[$params])){
					// add element to layout
					$layout[] = (object)[
						'type' => 'element',
						'element'=> $elements[$params]
					];
				} else {
					// add markup element to layout
					$layout[] = (object)[
						'type' => 'markup',
						'markup'=> (object) ['type' => $params]
					];
				}
			} elseif(is_array($params)) {
				// $params is an array
				// first element is the type
				if(isset($params[0])){
					$type = array_shift($params);
				} else {
					throw new Exception('Form layout item should be a string or an array with at least one item (type)');
				}

				// second element
				if(isset($params[0]) && is_array($params[0])) {
					// second element is a numerice array, this is a group
					// recursively build layout
					$group = array_shift($params);
					// add type to remaining params
					$params['type'] = $type;
					$params['elements'] = $this->buildLayout($group, $elements);
					$layout[] = (object)[
						'type' => 'group',
						'group' => (object) $params,
					];
				} elseif(! isset($params[0])) {
					// second element is not a numeric array
					// we'll presume it's markup with extra properties
					$params['type'] = $type;
					$layout[] = (object)[
						'type' => 'markup',
						'markup'=> (object) $params
					];
				}
			} else {
				throw new Exception('Form layout item should be a string or an array with at least one item (type)');
			}
		}
		return $layout;
	}
}
