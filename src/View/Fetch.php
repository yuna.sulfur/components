<?php

namespace Sulfur\View;

use Sulfur\View\API;

class Fetch extends API
{
	/**
	 * Class name.
	 * We can differentiate between the called class 'API' or 'Fetch'
	 * The first will mostly echo output, when fetch is called, it will return it.
	 * @var string
	 */
	protected static $class = 'fetch';
}