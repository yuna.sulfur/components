<?php

namespace Sulfur\View;

use Sulfur\View\API;
use Sulfur\View\Fetch;
use Sulfur\View\File;
use Exception;

class Engine
{
	/**
	 * File finder
	 * @var \Sulfur\View\Finder
	 */
	protected $finder;

	/**
	 * Found files that have readied for rendering
	 * @var array
	 */
	protected $files = [];

	/**
	 * Map to keep track of extending views
	 * @var array
	 */
	protected $map = [];

	/**
	 * Stack of nested blocks
	 * @var array
	 */
	protected $blocks = [];

	/**
	 * currently rendering file-id
	 * @var int
	 */
	protected $rendering = null;

	/**
	 * Shared data across all templates
	 * @var array
	 */
	protected $shared = [];

	/**
	 * Helper functions
	 * @var array
	 */
	protected $helpers = [];

	/**
	 * Assets that are accumulated in the template files
	 * @var array
	 */
	protected $assets = [];


	/**
	 * Create an engine
	 * @param Sulfur\View\Finder $finder
	 * @param string $view used for view:: calls in a template
	 * @param string $fetch used for fetch:: calls in a template
	 */
	public function __construct(Finder $finder, $view = 'view', $fetch = 'fetch')
	{
		// store finder
		$this->finder = $finder;

		// set aliases
		if(class_exists($view)) {
			if( ! method_exists($view, 'engine') && ! method_exists($view, 'extend')) {
				throw new Exception('Unable to use ' . $view . ' as an alias for the view api. Another class with this name exists');
			}
		} else {
			class_alias(API::class, $view);
		}

		if(class_exists($fetch)) {
			if( ! method_exists($fetch, 'engine') && ! method_exists($fetch, 'extend')) {
				throw new Exception('Unable to use ' . $fetch . ' as an alias for the fetch api. Another class with this name exists');
			}
		} else {
			class_alias(Fetch::class, $fetch);
		}

		// set this as the current engine in the api
		API::engine($this);
	}


	/**
	 * Add Helper or helpers
	 * @param string|array $nameOrHelpers
	 * @param mixed $helper
	 */
	public function helper($nameOrHelpers = null, $helper = null)
	{
		if($nameOrHelpers === null && $helper === null) {
			return $this->helpers;
		} elseif(is_string($nameOrHelpers) && $helper === null) {
			if(isset($this->helpers[$nameOrHelpers])) {
				return $this->helpers[$nameOrHelpers];
			} else {
				return null;
			}
		} elseif(is_array($nameOrHelpers)) {
			$this->helpers = array_merge($this->helpers, $nameOrHelpers);
		} else {
			$this->helpers[$nameOrHelpers] = $helper;
		}

		$reserved = [
			'info',
			'extend',
			'block',
			'start',
			'end',
			'file',
			'text',
			'attr',
			'raw',
			'asset',
			'assets',
			'shared'
		];

		$intersect = array_intersect(array_keys($this->helpers), $reserved);
		if(count($intersect) > 0) {
			throw new Exception('Tried to register view helper(s) under a reserved name: '.implode(', ',$intersect).'. Reserverd names are: '.implode(', ',$reserved));
		}
	}


	/**
	 * Set a global value or values
	 * @param string|array $keyOrValues
	 * @param mixed $value
	 */
	public function share($keyOrValues, $value = null)
	{
		if (is_array($keyOrValues)) {
			foreach ($keyOrValues as $key => $value) {
				$this->shared[$key] = $value;
			}
		} else {
			$this->shared[$keyOrValues] = $value;
		}
	}


	/**
	 * Create a new View-file
	 * @param string $file
	 * @return \Sulfur\View\File
	 */
	public function template($file, $data = [])
	{
		// create a file
		$file = new File($this, $file, $data);

		// add it to the map
		$this->map[$file->id()] = [
			'file' => $file,
			'parent' => null
		];

		return $file;
	}




	/**
	 * Render a view
	 * @param int $id
	 * @param string $file
	 * @param array $data
	 * @param array $blocks
	 * @return string
	 */
	public function render($id, $file, array $data = [], array $blocks = [])
	{
		// remember currently rendering id and blocks
		$rendering = $this->rendering;
		$stack = $this->blocks;

		// set the current rendering id
		$this->rendering = $id;
		// stasrt with empty blocks for each render
		$this->blocks = [];

		// start rendering / collect blocks / extending
		$output = $this->capture($file, $data);

		// check if the view was extended from a parent
		if ($this->map[$this->rendering]['parent'] !== null) {
			// get parent file
			$parent = $this->map[$this->map[$this->rendering]['parent']]['file'];

			// overwrite blocks in parentfile with blocks from this file
			foreach ($blocks as $name => $block) {
				$parent->block($name, $block);
			}

			// render the parent, this will set the rendering_id to the parent's
			// will go deeper as subsequent calls to view::extend are done
			$output = $parent->render();
		}

		// restore previous rendering id and blocks
		$this->rendering = $rendering;
		$this->blocks = $stack;

		// done
		return $output;
	}


	/**
	 * Get info about the currently rendering file
	 * @return array
	 */
	public function info()
	{
		return [
			'file' => $this->map[$this->rendering]['file']->file(),
			'data' => $this->map[$this->rendering]['file']->data(),
			'shared' => $this->shared,
		];
	}


	/**
	 * Extends another view file
	 * @param string $file
	 */
	public function extend($file)
	{
		// create new view with current rendering data
		$parent = $this->template($file, $this->map[$this->rendering]['file']->data());

		// add its id to the currently rendering objct as 'parent' property
		$this->map[$this->rendering]['parent'] = $parent->id();
	}


	/**
	 * Start a content block
	 * @param string $name
	 */
	public function start($name)
	{
		// add info to the block stack
		$this->blocks[] = [
			'rendering' => $this->rendering,
			'name' => $name
		];
		// start buffering
		ob_start();
	}


	/**
	 * End the last started content block
	 */
	public function end()
	{
		// end buffering get the block
		$output = ob_get_contents();
		ob_end_clean();

		// get the block info
		$block = array_pop($this->blocks);

		// get the file in which the block was started and ended
		$file = $this->map[$this->rendering]['file'];

		// set the output as a block in the part
		// but only if it wasn't already set by a child part
		if (! $file->block($block['name'])) {
			$file->block($block['name'], $output);
		}

		if (count($this->blocks) > 0) {
			// blocks-stack is not empty: this is a nested block
			// the output should end up here
			echo $file->block($block['name']);
		} else {
			if ($this->map[$block['rendering']]['parent'] !== null) {
				// stack is empty, but we are extending another part
				// set the output in the parent part
				$parent = $this->map[$this->map[$block['rendering']]['parent']]['file'];
				$parent->block($block['name'], $output);
			} else {
				// stack is empty, we are not extending
				// the output should end up here
				echo $file->block($block['name']);
			}
		}
	}


	/**
	 * Get a shared value
	 * @param string $name
	 * @return mixed
	 */
	public function shared($name = null)
	{
		if($name === null) {
			return $this->shared;
		} elseif (isset($this->shared[$name])) {
			return $this->shared[$name];
		}
	}


	/**
	 * Add an asset to a group
	 * @param string $group
	 * @param string $html
	 * @param boolean $duplicate overwrite or append duplicates
	 */
	public function asset($group, $html = '', $duplicate = false)
	{
		// create group array
		if (!isset($this->assets[$group])) {
			$this->assets[$group] = [];
		}
		// add html to group array
		if (!in_array($html, $this->assets[$group]) || $duplicate) {
			$this->assets[$group][] = $html;
		}
	}


	/**
	 * Get all assets
	 * @param string $group get assets for a certain group
	 * @return array
	 */
	public function assets($group = null)
	{
		if ($group === null) {
			return $this->assets;
		} elseif (isset($this->assets[$group])) {
			return $this->assets[$group];
		} else {
			return [];
		}
	}


	/**
	 * Render a file
	 * @param string $file
	 * @param array $data
	 * @return string
	 * @throws Exception
	 */
	protected function capture($file, $data)
	{
		// find the file and store the contents. Do this only once for each file
		if (!isset($this->files[$file])) {
			if ($path = $this->finder->__invoke($file)) {
				$this->files[$file] = [
					'path' => $path,
					'function' => null
				];
			} else {
				$this->files[$file] = false;
				return '';
			}
		} elseif($this->files[$file] === false) {
			return '';
		}

		// Start capture the view output
		if ($this->files[$file]['function'] === null) {
						// run the file as an include to capture errors the first time around
			// they wont we captured nicely when coming from a evaled closure
			// run it in a closure to protect the local variables
			$function = function(& $__data__, & $__file__) {
				extract($__data__, EXTR_OVERWRITE);
				include($__file__);
			};
			ob_start();
			$function($data, $this->files[$file]['path']);
			$html = ob_get_clean();

			// after that use a function with the contents
			// do this so we dont need includes. They are much slower
			eval('$function = function(& $__data__) {' .
			'extract($__data__, EXTR_OVERWRITE);' .
			' ?> ' . file_get_contents($this->files[$file]['path']) . ' <?php ' .
			'};');
			$this->files[$file]['function'] = $function;
		} else {
			// use the created function to render the contents
			ob_start();
			$this->files[$file]['function']($data);
			$html = ob_get_clean();
		}
		// return the result
		return $html;
	}
}