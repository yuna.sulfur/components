<?php

namespace Sulfur\View;

class Finder
{
	/**
	 * Found files
	 * Static, so every instance uses this array
	 * @var array
	 */
	protected static $found = [];

	/**
	 * Paths to check
	 * @var array
	 */
	protected $paths = [];


	/**
	 * Create a finder with a set of paths
	 * @param type $paths
	 */
	public function __construct($paths = [])
	{
		$this->paths = $paths;
	}


	/**
	 * Shortcut function, that uses the given paths
	 * @param string $file
	 * @return string|boolean
	 */
	public function __invoke($file)
	{
		return $this->find($file, $this->paths);
	}



	/**
	 * Find a file, by going through all the provided directories
	 * @param string $file
	 * @param string|array $path
	 * @param string $ext
	 * @param boolean $all Get all files; dont stop at the first encounter
	 * @return string|array|boolean
	 */
	protected function find($file, $paths = [])
	{
		// force array
		if(! is_array($paths)) {
			$paths = [$paths];
		}

		// try to get hotcached path
		$key = implode(';', $paths) . '_' . $file;
		if (isset(self::$found[$key])) {
			return self::$found[$key];
		}

		// find file
		$found = [];
		foreach ($paths as $path) {
			$full = $path . $file . '.php';
			if (file_exists($full)) {
				// cache found file
				self::$found[$key] = $full;
				// return the path
				return $full;
			}
		}

		// still here: tried to find path and failed
		return false;
	}
}

