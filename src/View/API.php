<?php

namespace Sulfur\View;

use Sulfur\View\Engine;

class API
{
	/**
	 * Instance of the current engine being used
	 * @var Sulfur\View\Engine
	 */
	static protected $engine;

	/**
	 * The class identifier under which the methods are called
	 * Fetch class extends API, $class will be 'fetch' then
	 * @var string
	 */
	static protected $class = 'view';


	/**
	 * Set an instance of Engine
	 * @param Sulfur\View\Engine $engine
	 */
	public static function engine(Engine $engine)
	{
		static::$engine = $engine;
	}


	/**
	 * View the info about the current render-status
	 * @return array
	 */
	public static function info()
	{
		if (static::$class === 'fetch') {
			return static::$engine->info();
		} else {
			echo var_dump(static::$engine->info());
		}
	}


	/**
	 * Extend a different view file
	 * @param string $file
	 */
	public static function extend($file)
	{
		static::$engine->extend($file);
	}


	/**
	 * Shorthand for start(); $content; end();
	 * @param string $name
	 * @param string $content
	 */
	public static function block($name, $content = '')
	{
		static::$engine->start($name);
		echo $content;
		static::$engine->end();
	}


	/**
	 * Start the rendering of a contentblock
	 * @param string $name
	 */
	public static function start($name)
	{
		static::$engine->start($name);
	}


	/**
	 * Complete the rendering of a contentblock
	 */
	public static function end()
	{
		static::$engine->end();
	}


	/**
	 * Add asset to specific asset group
	 * @param string $group
	 * @param string $html
	 * @param boolen $duplicate push to assets regardless of element existing
	 */
	public static function asset($group, $html = '', $duplicate = false)
	{
		static::$engine->asset($group, $html, $duplicate);
	}


	/**
	 * Get accumulated assets, provide no group for all assets
	 * @param string $group
	 * @return array
	 */
	public static function assets($group = null)
	{
		return static::$engine->assets($group);
	}


	/**
	 * get escaped text
	 * @param string $string
	 * @return string
	 */
	public static function text($string)
	{
		if (static::$class === 'fetch') {
			return htmlspecialchars($string);
		} else {
			echo htmlspecialchars($string);
		}
	}


	/**
	 * Get escaped html attribute
	 * @param string $string
	 * @return string
	 */
	public static function attr($string)
	{
		if (static::$class === 'fetch') {
			return htmlspecialchars($string, ENT_QUOTES);
		} else {
			echo htmlspecialchars($string, ENT_QUOTES);
		}
	}


	/**
	 * echo the string as is
	 * @param string $string
	 * @return string
	 */
	public static function raw($string)
	{
		if (static::$class === 'fetch') {
			return $string;
		} else {
			echo $string;
		}
	}


	/**
	 * Create and render a subview
	 * @param string $file
	 * @param array $data
	 * @return string|void
	 */
	public static function file($file, array $data = [])
	{
		// return / echo it
		if (static::$class === 'fetch') {
			return static::$engine->template($file)->render($data);
		} else {
			echo static::$engine->template($file)->render($data);
		}
	}


	/**
	 * Get a shared variable
	 * @param string $name
	 * @return mixed
	 */
	public static function shared($name = null)
	{
		$shared = static::$engine->shared($name);

		if (static::$class === 'fetch' || ! is_scalar($shared)) {
			return $shared;
		} else {
			echo $shared;
		}
	}


	/**
	 * Use a helper
	 * @param string $name
	 * @param array $args
	 * @return mixed
	 */
	public static function __callStatic($name, array $args)
	{
		if ($helper = static::$engine->helper($name)) {
			// use the helper
			if (is_object($helper) && method_exists($helper, '__invoke')) {
				// helper is a closue: call it and return the result
				$result = $helper(...$args);
				
				if (static::$class === 'fetch' || ! is_scalar($result)) {
					return $result;
				} else {
					echo $result;
				}
			} elseif (is_scalar($helper)) {
				// scalars are ok to output
				if (static::$class === 'fetch') {
					return $helper;
				} else {
					echo $helper;
				}
			} else {
				// helper is something else (probably an object) just return it
				return $helper;
			}
		} else {
			return null;
		}
	}
}