<?php

namespace Sulfur;

use Sulfur\Form\Form as BaseForm;

class Form
{
	/**
	 * Get a Sulfur\Form\Form with the provided builder-name and dependencies
	 * @param string $builder
	 * @param array $dependencies
	 * @return Sulfur\Form\Form
	 */
	public function get($builder = null, array $dependencies = [])
	{
		if (is_string($builder)) {
			$builder = new $builder($dependencies);
		}
		return new BaseForm($builder);
	}
}