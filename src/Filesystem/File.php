<?php

namespace Sulfur\Filesystem;

use League\Flysystem\File as BaseFile;

class File
{

	/**
	 * Flysystem handler
	 * @var League\Flysystem\File
	 */
	protected $fle;


	public function __construct(BaseFile $file)
	{
		$this->file = $file;
	}


    public function exists()
    {
        return $this->file->exists();
    }


    public function read()
    {
		return $this->file->read();
    }


    public function readStream()
    {
		return $this->file->readStream();
    }


    public function write($content)
    {
		return $this->file->write($content);
    }

    public function writeStream($resource)
    {
		return $this->file->writeStream($resource);
    }


    public function update($content)
    {
		return $this->file->update($content);
    }

    public function updateStream($resource)
    {
		return $this->file->updateStream($resource);
    }


    public function put($content)
    {
		return $this->file->put($content);
    }


    public function putStream($resource)
    {
		return $this->file->putStream($resource);
    }


    public function rename($path)
    {
		return $this->file->rename($path);
    }


    public function copy($newpath)
    {
		return $this->file->copy($path);
    }


    public function timestamp()
    {
		return $this->file->getTimestamp();
    }


    public function mimetype()
    {
        return $this->file->getMimetype();
    }


    public function visibility()
    {
        return $this->file->getVisibility();
    }


    public function metadata()
    {
        return $this->file->getMetadata();
    }

    public function size()
    {
        return $this->file->getSize();
    }

    public function delete()
    {
        return $this->file->delete();
    }
}