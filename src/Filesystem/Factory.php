<?php

namespace Sulfur\Filesystem;

use Sulfur\Filesystem;

class Factory
{

	protected static $filesystems = [];

	public static function make(array $config, $name = null)
	{
		if($name === null) {
			$name = key($config);
		}
		$config = $config[$name];

		if(!isset(self::$filesystems[$name])) {
			self::$filesystems[$name] = new Filesystem($config);
		}
		return self::$filesystems[$name];
	}
}