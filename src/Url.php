<?php

namespace Sulfur;

use Sulfur\Router;
use Sulfur\Request;

class Url
{

	protected $router;

	protected $request;

	public function __construct(Router $router, Request $request)
	{
		$this->router = $router;
		$this->request = $request;
	}


	public function base($parts = [])
	{
		// build base url
		$scheme = isset($parts['scheme']) ? $parts['scheme'] : $this->request->scheme();
		$domain = isset($parts['domain']) ? $parts['domain'] : $this->request->domain();

		if(isset($parts['port'])) {
			$port = (string) $parts['port'];
		} elseif ($scheme === 'https') {
			$port = 443;
		} else {
			$port = $this->request->port();
		}
		$port = ($port === 80 || $port === 443) ? '' : ':'.$port;

		$basePath = trim($this->request->base(), '/');
		if($basePath !== '') {
			$basePath = $basePath . '/';
		}

		$base = ($scheme !== '' ? $scheme . '://' : '//') . $domain . $port. '/' . $basePath;

		return $base;
	}


	public function current($qs = true)
	{
		$url = rtrim($this->base(), '/') . '/' . ltrim($this->request->path(false), '/');
		if($qs && $string = $this->request->qs()) {
			$url .= '?' . $string;
		}
		return $url;
	}


	public function route($route, array $params = [], array $parts = [])
	{
		if (isset($parts['base'])) {
			// get base from components
			$base = $parts['base'];
		} else {
			$base = $this->base();
		}
		// get path
		$path = isset($parts['path']) ? $parts['path'] : $this->router->path($route, $params);
		// query string
		$query = isset($parts['query']) ? '?' . $parts['query'] : '';
		// get fragment
		$fragment = isset($parts['fragment']) ? '#' . $parts['fragment'] : '';
		// done
		return  rtrim($base, '/') . '/' . ltrim($path, '/') . $query . $fragment;
	}
}