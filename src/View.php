<?php

namespace Sulfur;

use Sulfur\View\Finder;
use Sulfur\View\Engine as Engine;

class View
{
	protected $config = [
		'paths' => [],
		'view' => 'view',
		'fetch' => 'fetch'
	];


	public function __construct($config)
	{
		$config = array_merge($this->config, $config);
		$this->engine = new Engine(new Finder($config['paths'], $config['view'], $config['fetch']));
	}

	public function template($file)
	{
		return $this->engine->template($file);
	}

	public function render($file, array $vars = [])
	{
		return $this->engine->template($file)->render($vars);
	}

	public function helper($nameOrHelpers = null, $helper = null)
	{
		$this->engine->helper($nameOrHelpers, $helper);
	}

	public function share($keyOrValues, $value = null)
	{
		$this->engine->share($keyOrValues, $value);
	}
}