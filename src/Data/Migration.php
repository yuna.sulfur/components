<?php

namespace Sulfur\Data;

use Sulfur\Data;
use Sulfur\Data\Relation;

class Migration
{
	/**
	 * Data object needed to create relations
	 * @var \Sulfur\Data
	 */
	protected $manager;

	/**
	 * Path that glob() can use to locate potential Entity files
	 * @var array
	 */
	protected $paths = [];

	/**
	 * Create a Migration helper
	 * @param \Sulfur\Data $manager
	 * @param array $paths
	 */
	public function __construct(Data $manager, $paths)
	{
		$this->manager = $manager;

		if(! is_array($paths)){
			$paths = [$paths];
		}
		$this->paths = $paths;
	}


	/**
	 * Create a schema by looking at all the qualifying files in $this->paths
	 * The result will look like
	 * [
	 *		'db1' => [
	 *			'table1' => [
	 *				'column1' =>[
	 *					'type' => [type],
	 *					'length' => [length]
	 *					.
	 *					.
	 *				],
	 *				.
	 *				.
	 *			],
	 *			.
	 *			.
	 *		],
	 *		.
	 *		.
	 *	]
	 * @return array
	 */
	public function schema($classes = null)
	{
		if(is_array($classes)) {
			// specific classes given
			$entities = [];
			foreach($classes as $entity) {
				// check if it's an entity table by testing for static table and columns methods
				if(is_callable($entity . '::table')  && is_callable($entity . '::columns') ){
					// get the table of the entity and make sure only one entity is using the table (so the table wont be created twice)
					$table = call_user_func($entity . '::table');
					if($table && ! isset($entities[$table])) {
						$entities[$table] = $entity;
					}
				}
			}
		} else {
			// get all the entity files in the paths
			// we use glob() so we can use wildcards in the paths
			$files = [];
			foreach($this->paths as $path){
				$files = array_merge($files, glob($path));
			}

			// Extract entities from the found files
			$entities = [];
			foreach($files as $file){
				// a list of defined classes before we include this file
				$classes = get_declared_classes();

				// include the file
				try {
					// only include once because:
					// When the class extends some other class and that other class is also found
					// it won't be included here, therefore not resulting in an error
					//
					include_once($file);
				} catch (Exception $e) {
					continue;
				}
				// Get the extra classes that were loaded by including the file
				$loaded = array_diff(get_declared_classes(), $classes);

				if($loaded && count($loaded) > 0) {
					// There could be more than one class loaded because of implements or extends
					// we want to stasrt with the last extension first
					$loaded = array_reverse($loaded);
					foreach($loaded as $entity) {
						// check if it's an entity table by testing for static table and columns methods
						if(is_callable($entity . '::table')  && is_callable($entity . '::columns') ){
							// get the table of the entity and make sure only one entity is using the table (so the table wont be created twice)
							$table = call_user_func($entity . '::table');
							if($table && ! isset($entities[$table])) {
								$entities[$table] = $entity;
							}
						}
					}
				}
			}
		}

		// Build array with schema
		$schema = [];
		foreach($entities as $entity) {
			// Get the entity properties
			$database = call_user_func($entity . '::database');
			$table = call_user_func($entity . '::table');
			$columns = call_user_func($entity . '::columns');
			$relations = call_user_func($entity . '::relations');

			// put it in the correct database
			if(! isset($schema[$database])) {
				$schema[$database] = [];
			}

			// build a proper table schema and store it
			$schema[$database][$table] = $this->table($columns);

			// Check if there are junction tables to craete
			foreach($relations as $name => $params){
				// create a relation to extract info from
				$relation = new Relation($entity, $name, $this->manager);


				if($junction = $relation->junction()){
					// get schema for junction table
					// junction table will always be set in the same database as the entity table
					// so with many to many in two different databases, there will be two junction tables
					$table = [
						'id' => 'int',
						$relation->left() => 'int',
						$relation->right() => 'int'
					];
					// if the junction is polymorphic, add the indexed poly field
					if($poly = $relation->poly()) {
						$table[$poly] = ['string', 'index' => true];
					}

					// create full table data
					$table = $this->table(array_merge($table, $relation->columns()));

					if(isset($schema[$database][$junction])) {
						// junction table is already there!
						// It is declared in both relating entities
						// one of them might hold extra columns, so dont simply overwrite
						// instead merge
						$schema[$database][$junction] = array_replace_recursive($schema[$database][$junction], $table);
					} else {
						$schema[$database][$junction] = $table;
					}
				}
			}
		}
		return $schema;
	}


	/**
	 * Create a complete table schema from columns that are defined in a Sulfur\Entity
	 * @param array $columns
	 * @return array
	 * @throws Exception
	 */
	protected function table($columns)
	{
		// the value of the 'after' field. Is false for the first column
		// is updated to the previous column at the end of the foreach
		$after = false;

		$table = [];
		foreach($columns as $column => $params) {

			// it's possible to define only a type (string)
			// cast it to an array here
			if(! is_array($params)) {
				$params = [$params];
			}


			// try to extract type. It can be the first in an array or under the key 'type'
			$type = isset($params[0]) ? $params[0] : (isset($params['type']) ? $params['type'] : false);


			// we need a type
			if(! $type) {
				throw new Exception('No type declared for column ' . $column);
			}

			// Create default values by given type
			$length = null;
			$values = null;
			$null = true;
			$default = null;
			$increment = false;
			$index = false;
			$unique = false;
			switch($type) {
				case 'boolean':
					$length = 4;
					$null = false;
					$default = 0;
					break;
				case 'int':
					$length = 11;
					$null = false;
					$default = 0;
					break;
				case 'float':
					$null = false;
					$default = 0;
					break;
				case 'string':
					$length = 255;
					$null = false;
					$default = '';
					break;
				case 'enum':
					$values = [];
					break;
			}


			if($column === 'id') {
				// Column 'id' always acts a PK
				$index = true;
				$increment = true;
				$unique = true;
			} elseif(substr($column, -3) === '_id') {
				// columns with an _id postfix always get an index
				$index = true;
			}

			// Create final column schema by overwriting the default with the given params
			$table[$column] = [
				'type' => $type,
				'length' => isset($params['length']) ? $params['length'] : $length,
				'values' => isset($params['values']) ? $params['values'] : $values,
				'null' =>  isset($params['null']) ? $params['null'] : $null,
				'default' => isset($params['default']) ? $params['default'] : $default,
				'increment' =>  isset($params['increment']) ? $params['increment'] : $increment,
				'index' => isset($params['index']) ? $params['index'] : $index,
				'unique' => isset($params['unique']) ? $params['unique'] : $unique,
				'after' => $after,
			];

			// keep track of the 'after' field
			$after = $column;
		}
		return $table;
	}


	/**
	 * Calculate a diff between two outputs of ::schema
	 * @param array $old
	 * @param array $new
	 * @return array
	 */
	public function diff(array $old, array $new)
	{
		// start with empty diff
		$diff = [];

		// get combined database names in old and new
		$databases = array_unique(array_merge(array_keys($old), array_keys($new)));

		// go through each database to find add, removed, renamed and changed tables
		foreach($databases as $database){

			// make sure databases exist in both old and new
			$old[$database] = isset($old[$database]) ? $old[$database] : [];
			$new[$database] = isset($new[$database]) ? $new[$database] : [];
			// tables to create are not present in old
			$create = array_diff_key($new[$database], $old[$database]);
			// tables to drop are not presen in new
			$drop = array_diff_key($old[$database], $new[$database]);
			// check if we need to rename instead of drop / create
			$rename = [];
			foreach($drop as $name1 => $columns1){
				foreach($create as $name2 => $columns2){
					// check if there is a table in drop that is identical in create
					// if so: only a name change is needed
					if($this->identicalTables($columns1, $columns2)){
						// move the table from drop and create to rename
						$rename[$name1] = $name2;
						unset($drop[$name1]);
						unset($create[$name2]);
						break;
					}
				}
			}
			// store create/drop/rename
			if($create) {
				$diff[$database]['create'] = $create;
			}
			if($drop) {
				$diff[$database]['drop'] = array_keys($drop);
			}
			if($rename) {
				$diff[$database]['rename'] = $rename;
			}

			// get the tables that are in both old and new
			$tables = array_intersect_key($new[$database], $old[$database]);

			// go through each table to find added, removed, renamed and changed columns
			foreach($tables as $table => $columns){
				// columns to create are not present in old
				$create = array_diff_key($columns, $old[$database][$table]);
				// columns to drop are not present in new
				$drop = array_diff_key($old[$database][$table], $columns);
				// check if we need to rename instead of drop/create
				$rename = [];
				foreach($drop as $name1 => $properties1){
					foreach($create as $name2 => $properties2){
						// check if there is a column in drop that is identical in create
						// if so: only a name change is needed
						if($this->identicalColumns($properties1, $properties2)){
							// move the column from drop and create to rename
							$rename[$name1] = $name2;
							unset($drop[$name1]);
							unset($create[$name2]);
							break;
						}
					}
				}
				// store create/drop/rename
				if($create) {
					$diff[$database]['alter'][$table]['create'] = $create;
				}
				if($drop) {
					$diff[$database]['alter'][$table]['drop'] = array_keys($drop);
				}
				if($rename) {
					$diff[$database]['alter'][$table]['rename'] = $rename;
				}

				// get the columns that are in both old and new
				$columns = array_intersect_key($columns, $old[$database][$table]);

				// go through each column to find changed properties
				foreach($columns as $column => $properties) {
					foreach($properties as $property => $value){
						if(!key_exists($property, $old[$database][$table][$column]) || $value !== $old[$database][$table][$column][$property]) {
							$diff[$database]['alter'] [$table]['alter'][$column][$property] = $value;
						}
					}
				}
			}
		}

		return $diff;
	}


	/**
	 * Check if tables are identical by comparing the column names
	 * @param array $columns1
	 * @param array $columns2
	 * @return coolean
	 */
	protected function identicalTables($columns1, $columns2)
	{
		return join(',', array_keys($columns1)) === join(',', array_keys($columns2));
	}


	/**
	 * Check if columns are identical by comparing all the properties
	 * @param array $column1
	 * @param array $column2
	 * @return boolean
	 */
	protected function identicalColumns($column1, $column2)
	{
		return (
			$column1['type'] === $column2['type']
			&& $column1['length'] === $column2['length']
			&& $column1['null'] === $column2['null']
			&& $column1['default'] === $column2['default']
			&& $column1['increment'] === $column2['increment']
			&& $column1['index'] === $column2['index']
			&& $column1['unique'] === $column2['unique']
			&& $column1['after'] === $column2['after']
		);
	}
}
