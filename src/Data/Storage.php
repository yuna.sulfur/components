<?php

namespace Sulfur\Data;

use Sulfur\Data;
use Sulfur\Data\Relation;
use Sulfur\Data\Entity;

class Storage
{

	/**
	 * The central data object
	 * @var \Sulfur\Data
	 */
	protected $manager;


	/**
	 * New Data storage
	 * @param \Sulfur\Data $manager
	 */
	public function __construct(Data $manager)
	{
		$this->manager = $manager;
	}


	/**
	 * Persist an entity and set given relations
	 * Relations should be passed as follows ['<relationName>' => [<relatives>], ...]
	 *
	 * @param \Sulfur\Data\Entity $entity
	 * @param array $relatives
	 */
	public function save(Entity $entity, array $relations = [])
	{
		// prepare dirty data to update or insert
		$prepared = $this->prepare($entity->dirty(), $entity::columns());

		// get the correct database
		$database = $this->manager->database($entity::database());

		if($entity->id && count($prepared) > 0) {
			// update entity
			$database->update($entity::table())
			->set($prepared)
			->where('id', $entity->id)
			->execute();
		} elseif(! $entity->id) {
			// create entity and set id
			$entity->id = $database->insert($entity::table())
			->values($prepared)
			->result();
		}

		//update relations if they are present
		foreach($relations as $name => $relatives){
			$this->set(new Relation($entity, $name, $this->manager), $relatives);
		}
	}


	/**
	 * Delete an entity with its relations
	 * @param \Sulfur\Data\Entity $entity
	 */
	public function delete(Entity $entity)
	{
		if($entity->id !== null && $entity->id > 0){

			// get the correct database
			$database = $this->manager->database($entity::database());

			// remove relations for entity
			foreach($entity::relations() as $name => $params){
				$this->remove(new Relation($entity, $name, $this->manager));
			}

			// remove entity itself
			$database->delete($entity::table())
			->where('id', $entity->id)
			->execute();

			// unset id
			$entity->id = null;
		}
	}


	/**
	 * Set a new collection of related items in a relation
	 * @param \Sulfur\Data\Relation $relation
	 * @param array $relatives
	 */
	public function set(Relation $relation, $relatives = [])
	{
		// get normalized new relatives
		$new = $this->relatives($relatives);
		$type = $relation->type();
		// only used for polymorphic relations
		$poly = $relation->poly();
		$related = $relation->related();

		if($type === 'junction' || $type === 'set') {
			// get the origin
			$origin = $relation->origin();

			// get the correct databases
			$originDatabase = $this->manager->database($origin::database());
			$relationDatabase = $this->manager->database($relation->database());

			// check if origin has a set
			if($relation->type() === 'set' &&  (int) $origin->{$relation->from()} === 0) {
				// origin doesnt have a set yet
				// create one and get the new setId with -1, -1 values
				$setId = $relationDatabase->insert($relation->junction())
				->values([
					$relation->left() => -1,
					$relation->right() => -1,
				])
				->result();

				// set setId in the origin entity
				$originDatabase->update($origin::table())
				->set([
					$relation->from() => $setId
				])
				->where('id', $origin->id)
				->execute();

				// update the entity value
				$origin->{$relation->from()} = $setId;
			}

			// get existing relative entities
			$query = $relationDatabase->select('id')
			->from($relation->junction())
			->where($relation->left(), $origin->{$relation->from()});

			$existing = $query->result();

			// first use all of the existing rows to store the give relatives
			// use the junction id to target the existing row
			for($i = 0; $i < min(count($existing), count($new)); $i++) {
				// prepared junctiondata of new item
				$junctionData = $this->prepare($new[$i], $relation->columns());

				// add relevant key data
				$values = array_merge($junctionData, [
					$relation->right() => $new[$i]['id'],
				]);

				// update the relation row
				$result = $relationDatabase->update($relation->junction())
				->values($values)
				->where('id', $existing[$i]['id'])
				->execute();
			}

			// difference between the number of old and new relatives
			$diff = count($existing) - count($new);

			if($diff > 0) {
				// more existing relatives than new ones: remove the extra rows by their junction id
				$result = $relationDatabase->delete($relation->junction())
				->where('id', 'in', array_column(array_slice($existing, count($existing) - $diff), 'id'))
				->execute();
			} elseif($diff < 0) {
				// more new relatives than existing ones, add these
				$this->add($relation, array_slice($new, count($new) + $diff));
			}
		} else {
			// just remove all
			$this->remove($relation);

			// add new ones
			$this->add($relation, $new);
		}
	}


	/**
	 * Add one or more related items to a relation
	 * @param \Sulfur\Data\Relation $relation
	 * @param array $relatives
	 */
	public function add(Relation $relation, $relatives = [])
	{
		// normalize relatives data
		$relatives = $this->relatives($relatives);


		foreach($relatives as $relative) {

			// get the origin
			$origin = $relation->origin();

			// get the correct databases
			$originDatabase = $this->manager->database($origin::database());
			$relationDatabase = $this->manager->database($relation->database());

			// many to many types
			if($relation->type() === 'junction' || $relation->type() === 'set') {

				// get from value
				$fromValue = (int) $origin->{$relation->from()};

				// prepared junctiondata
				$junctionData = $this->prepare($relative, $relation->columns());

				// add relevant key data
				$values = array_merge($junctionData, [
					$relation->left() => $fromValue,
					$relation->right() => $relative['id'],
				]);

				// insert the relation
				$result = $relationDatabase->insert($relation->junction())
				->values($values)
				->execute();

			} elseif ($relation->type() == 'one' || $relation->type() == 'many') {
				$values = [
					$relation->to() => $origin->id
				];
				if ($poly = $relation->poly()) {
					$values[$poly] = $relation->related();
				}
				// update related entity keys
				$relationDatabase->update($relation->table())
				->values($values)
				->where('id', $relative['id'])
				->execute();
			} elseif ($relation->type() === 'belongs') {
				$values = [
					$relation->from() => $relative['id']
				];
				if ($poly = $relation->poly()) {
					$values[$poly] = $relation->related();
				}
				// update entity key
				$originDatabase->update($origin::table())
				->values($values)
				->where('id', $origin->id)
				->execute();
			}
		}
	}


	/**
	 * Remove one or more or all related items from a relation
	 * @param \Sulfur\Data\Relation $relation
	 * @param array $related
	 */
	public function remove(Relation $relation, $relativesOrAll = true)
	{
		// get the origin
		$origin = $relation->origin();

		// get the correct database
		$originDatabase = $this->manager->database($origin::database());
		$relationDatabase = $this->manager->database($relation->database());

		// get relatedIds from related
		if($relativesOrAll === true) {
			$all = true;
			$ids = [];
		} else {
			// normalize related & get ids
			$all = false;
			$ids = array_column($this->relatives($relativesOrAll), 'id');
		}

		if($relation->type() === 'junction' || $relation->type() === 'set') {
			// delete from junction table
			$query = $relationDatabase->delete($relation->junction())
			->where($relation->left(), $origin->{$relation->from()});

			if($all) {
				// delete all
				$query->execute();
			} elseif(count($ids) > 0) {
				// only delete specified
				$query->where($relation->right(), 'in', $ids)
				->execute();
			}

		} elseif ($relation->type() === 'one' || $relation->type() === 'many') {
			// set foregin key of related to 0 of all related to origin
			$query = $relationDatabase->update($relation->table())
			->values([
				$relation->to() => 0
			])
			->where($relation->to(), $origin->id);

			// filter polymorphic relations
			if($poly = $relation->poly()) {
				$query->values([
					$poly => ''
				])
				->where($poly, $relation->related());
			}

			if($all) {
				// delete all
				$query->execute();
			} elseif(count($ids) > 0) {
				// only delete specified
				$query->where('id', 'in', $ids)
				->execute();
			}

		} elseif ($relation->type() === 'belongs') {
			// set foregin key of origin to 0
			$query = $originDatabase->update($origin::table())
			->values([
				$relation->from() => 0
			])
			->where('id', $origin->id);

			// filter on and reset polymorphic relations
			if($poly = $relation->poly()) {
				$query->values([
					$poly => ''
				])
				->where($poly, $relation->related());
			}
			// update
			$query->execute();
		}
	}


	/**
	 * Parse a given value for related items to the form
	 *
	 * [
	 *		['id' => <relatedId>,  '<junctionvar>' => <junctionval>, ...],
	 *		.
	 *		.
	 * ]
	 *
	 * Input can be of the following forms:
	 *
	 * 1:
	 * <relatedId>
	 *
	 * 2:
	 * [<relatedId>, <relatedId>, ...]
	 *
	 * 3:
	 * [<relatedId>, '<junctionvar>' => <junctionval>, ...]
	 *
	 * 4:
	 * ['id' => <relatedId>, '<junctionvar>' => <junctionval>, ...]
	 *
	 *
	 * 5:
	 * [
	 *		[<relatedId>, '<junctionvar>' => <junctionval>, ...],
	 *		.
	 *		.
	 * ]
	 *
	 *
	 * 6:
	 * [
	 *		['id' => <relatedId>, '<junctionvar>' => <junctionval>, ...],
	 *		.
	 *		.
	 * ]
	 *
	 * 7:
	 * [
	 *		<relatedId> => ['<junctionvar>' => <junctionval>, ...],
	 *		.
	 *		.
	 * ]
	 *
	 * 8:
	 * <Entity>
	 *
	 * 9:
	 * [<Entity>, <Entity>, ...]
	 *
	 * In the last case junction data will be retrieved from the junction() method
	 *
	 * @param array $related
	 * @return array
	 * @throws \Exception
	 */
	public function relatives($input)
	{

		// always put relatives in array
		if(! is_array($input)) {
			// input type #1 or #8
			$relatives = [$input];
		} elseif( array_keys(array_keys($input)) !== array_keys($input)) {
			// array with irregual keys
			if(isset($input['id']) || isset($input[0])) {
				// input type #3 or #4
				$relatives = [$input];
			} else {
				// input type #7
				$relatives = $input;
			}
		} else {
			// input type #2, #5, #6, #9
			$relatives = $input;
		}

		$parsed = [];
		foreach($relatives as $key => $relative) {
			$data = [];
			if((is_int($relative) || is_numeric($relative))) {
				$data['id'] = $relative;
			} elseif($relative instanceof Entity) {
				$data = $relative->junction();
				$data['id'] = $relative->id;
			} elseif(is_array($relative)) {

				$data = $relative;
				if(isset($data[0])){
					$data['id'] = $data[0];
				} elseif(! isset($data['id'])) {
					$data['id'] = $key;
				}
			}

			if( ! isset($data['id'])){
				continue;
			}
			$parsed[] = $data;
		}
		return $parsed;
	}


	/**
	 * Prepare and cast columns
	 * @param array $data
	 * @param array $columns
	 * @return array
	 */
	protected function prepare(array $data, array $columns)
	{
		$prepared = [];
		foreach($columns as $column => $params){
			if(isset($data[$column])) {
				// determine type
				if(is_string($params)) {
					$type = $params;
				} elseif(is_array($params)) {
					$type = isset($params[0]) ? $params[0] : (isset($params['type']) ? $params['type'] : null);
				} else {
					$type = null;
				}
				switch ($type) {
					case 'int':
					case 'integer':
						$prepared[$column] = (int) $data[$column];
						break;
					case 'float':
					case 'decimal':
						$prepared[$column] = (float) $data[$column];
						break;
					case 'date':
						$prepared[$column] = date('Y-m-d', strtotime($data[$column]));
						break;
					case 'datetime':
					case 'timestamp':
						$prepared[$column] = date('Y-m-d H:i:s', strtotime($data[$column]));
						break;
					case 'boolean':
					case 'bool':
						$prepared[$column] = $data[$column] ? 1 : 0;
						break;
					case 'json':
						$prepared[$column] = json_encode($data[$column], JSON_UNESCAPED_UNICODE);
						break;
					default:
						$prepared[$column] = $data[$column];
				}
			}
		}
		return $prepared;
	}
}