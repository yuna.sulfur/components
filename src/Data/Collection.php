<?php

namespace Sulfur\Data;

use Sulfur\Data;
use Iterator;
use Countable;
use PDO;

class Collection implements Iterator, Countable
{
	/**
	 * Array of entities
	 * @var array
	 */
	protected $entities = [];

	/**
	 * The entities mapped by certain properties
	 * @var array
	 */
	protected $maps = [];

	/**
	 * Whether all the entities are mapped
	 * @var boolean
	 */
	protected $mapped = true;

	/**
	 * Iterator implementation, current position
	 * @var int
	 */
	protected $position = 0;

	/**
	 * Iterator implementation, current entity
	 * @var \Sulfur\Data\Entity
	 */
	protected $current = false;

	/**
	 * Cache counted elements
	 */
	protected $count = null;


	/**
	 * Create a collection
	 * @param string $class Entity class
	 * @param \Iterator $iterator PDO iterator
	 * @param \Sulfur\Data $manager
	 */
	public function __construct($class, $iterator, Data $manager)
	{
		$this->class = $class;
		$this->iterator = $iterator;
		$this->manager = $manager;
	}


	/**
	 * Get the first entity
	 * @return \Sulfur\Data\Entity|null
	 */
	public function first()
	{
		$this->rewind();
		return $this->current;
	}


	/**
	 * Get a flat array
	 * @return array
	 */
	public function flat($key = null, $value = null)
	{
		$result = [];
		foreach($this as $entity) {
			if ($key === null) {
				if ($value === null) {
					$result[] = $entity;
				} else {
					$result[] = $entity->{$value};
				}
			} else {
				if ($value === null) {
					$result[$entity->{$key}] = $entity;
				} else {
					$result[$entity->{$key}] = $entity->{$value};
				}
			}
		}
		return $result;
	}


	/**
	 * Get an array of property values
	 *
	 * $names has the form
	 * [
	 *		$columnName => [$filterColumnName => $value, $filterColumnName => $value, ...]
	 *		$columnName => [$filterColumnName => $value, $filterColumnName => $value, ...]
	 * ]
	 *
	 * Filters are mainly used for plucking for polymorphic belongs and set relations
	 *
	 *
	 * @param string|array $names
	 * @return array
	 */
	public function pluck(array $names)
	{
		// create empty arrays for all the names that need to be plucked
		$plucked = array_fill_keys(array_keys($names), []);

		foreach($this as $entity){
			foreach($names as $name => $filter){
				// check filters
				$include = true;
				foreach($filter as $column => $value){
					if($entity->{$column} !== $value) {
						$include = false;
						break;
					}
				}
				if($include) {
					$plucked[$name][] = $entity->{$name};
				}
			}
		}
		return $plucked;
	}


	/**
	 * Map groups of entities under a certain property value
	 * @param string|array $names
	 */
	public function map($names)
	{
		// convert single name to array
		if(! is_array($names)) {
			$names = [$names];
		}
		// set all the given names to empty array
		foreach($names as $name){
			$this->maps[$name] = [];
		}
		// flag mapped as not done
		$this->mapped = false;
	}


	/**
	 * Get a mapped group by value
	 * @param string $name
	 * @param mixed $value
	 * @return array
	 */
	public function mapped($name, $value)
	{
		if(! $this->mapped){
			// if not mapped, just run the iterator once.
			foreach($this as $entity);
		}
		if(isset($this->maps[$name]) && isset($this->maps[$name][$value])){
			// get the mapped array of entities
			return $this->maps[$name][$value];
		} else {
			// nothing here
			return [];
		}
	}


	/**
	 * Get the entity at a certain position
	 * @param int $position
	 * @return \Sulfur\Data\Entity|null
	 */
	protected function entity($position)
	{
		if(isset($this->entities[$position])) {
			// get a hydrated entity
			$entity = $this->entities[$position];
		} elseif($row = $this->iterator->fetch(PDO::FETCH_ASSOC)) {
			// create a fresh entity
			$class = $this->class;
			$entity =  new $class([], $this->manager);
			// set clean data (second argument is false)
			$entity->data($row, false);
			$this->entities[] = $entity;
		} else {
			// We reached the end and mapped everything
			$this->mapped = true;
			// not a valid entity
			$entity = null;
		}


		if($entity && ! $this->mapped) {
			// map the entity to various maps
			foreach($this->maps as $name => $map) {
				$value = $entity->__get($name);
				if(! isset($this->maps[$name][$value])){
					$this->maps[$name][$value] = [];
				}
				$this->maps[$name][$value][] = $entity;
			}
		}
		return $entity;
	}

	/**
	 * Countable implementation: count
	 * @returnint
	 */
	public function count()
	{
		if($this->count === null) {
			$count = 0;
			foreach($this as $entity) {
				$count++;
			}
			$this->count = $count;
		}
		return $this->count;
	}


	/**
	 * Iterator implementation: rewind
	 */
	public function rewind()
	{
		$this->position = 0;
		$this->current = $this->entity($this->position);
	}


	/**
	 * Iterator implementation: current
	 * @return \Sulfur\Data\Collection
	 */
	public function current()
	{
		return $this->current;
	}


	/**
	 * Iterator implementation: key
	 * Get the current position of the iterator
	 * @return int
	 */
	public function key()
	{
		return $this->position;
	}


	/**
	 * Iterator implementation: next
	 */
	public function next()
	{
		$this->position++;
		$this->current = $this->entity($this->position);
	}


	/**
	 * Iterator implementation: valid
	 * @return boolean
	 */
	public function valid()
	{
		return $this->current !== null;
	}
}