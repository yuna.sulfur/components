<?php

namespace Sulfur\Data;

use Sulfur\Database;
use Sulfur\Data;
use Sulfur\Data\Collection;
use Sulfur\Data\Relation;
use Sulfur\Data\Entity;

class Finder
{
	/**
	 * The entity class
	 * @var string
	 */
	protected $entity;

	/**
	 * Instance of the data manager
	 * @var \Sulfur\Data
	 */
	protected $manager;

	/**
	 * Database
	 * @var \Sulfur\Database
	 */
	protected $database;

	/**
	 * The used table
	 * @var string
	 */
	protected $table;

	/**
	 * The columns
	 * @var array
	 */
	protected $columns;

	/**
	 * The relations specs
	 * @var array
	 */
	protected $relations;

	/**
	 * Parent relation
	 * @var
	 */
	protected $relation;

	/**
	 * Eager load these relations
	 * @var array
	 */
	protected $with = [];

	/**
	 * Only load these columns or relations
	 * @var array
	 */
	protected $only = [];

	/**
	 * Omit these columns or relations
	 * @var array
	 */
	protected $without = [];

	/**
	 * Stack of SQL to pass to the query
	 * @var array
	 */
	protected $sql = [];


	/**
	 * Create a finder
	 * @param string $entity the Entity class
	 * @param Data $manager
	 */
	public function __construct($entity, Data $manager)
	{
		$this->entity = $entity;
		$this->manager = $manager;

		$this->table = $entity::table();
		$this->columns = $entity::columns();
		$this->relations = $entity::relations();
		$this->database = $manager->database($entity::database());
	}


	/**
	 * If the finder is created by a relation, it will register itself
	 * @param \Sulfur\Data\Relation $relation
	 */
	public function relation($relation)
	{
		$this->relation = $relation;
	}


	/**
	 * Tell finder to use another data
	 * Must be one that was provider to the manager
	 * @param string $name
	 */
	public function database($name)
	{
		$this->database = $manager->database($name);
	}


	/**
	 * Add directly to the SQL stack
	 * @param string $method
	 * @param array $args
	 * @return Finder
	 */
	public function __call($method, array $args)
	{
		$allowed = [
			'select',
			'distinct',
			'index',
			'join',
			'on',
			'oron',
			'onwhere',
			'oronwhere',
			'where',
			'orwhere',
			'group',
			'having',
			'orhaving',
			'limit',
			'offset',
			'order'
		];

		if(in_array(strtolower($method), $allowed)){
			// automatically prefix certain methods with column name
			// if there is no dot present in the column name
			$prefix = [
				'select',
				'distinct',
				'on',
				'oron',
				'onwhere',
				'oronwhere',
				'where',
				'orwhere',
				'group',
				'having',
				'orhaving',
				'order'
			];
			if(in_array(strtolower($method), $prefix) && isset($args[0])) {
				if($method === 'select' && is_array($args[0]) && isset($args[0][0]) && is_string($args[0][0]) && strpos($args[0][0], '.') === false){
					// argument is an aliased column name in a select ['column', 'alias'] construction
					$args[0][0] = $this->table . '.' . $args[0][0];
				} elseif(is_string($args[0]) && strpos($args[0], '.') === false) {
					// other column names without a dot
					$args[0] =  $this->table . '.' . $args[0];
				}
			}
			$this->sql[] = [$method, $args];
			return $this;
		} elseif($method === 'raw' && isset($args[0]) ) {
			// used __call as a gateway to database 'raw'
			return $this->database->raw($args[0]);
		}
	}


	/**
	 * Add a where clause to a junction table
	 * @param string $column
	 * @param string|mixed $operatorOrSecond
	 * @param mixed $second
	 * @return \Sulfur\Data\Finder
	 */
	public function whereJunction($column, $operatorOrSecond, $second = null)
	{
		$this->sql[] = ['having', ['junction:' . $column, $operatorOrSecond, $second]];
		return $this;
	}


	/**
	 * Add a sort to a junction table
	 * @param string $column
	 * @param string $direction
	 * @return \Sulfur\Data\Finder
	 */
	public function orderJunction($column, $direction = null)
	{
		$this->sql[] = ['order', ['junction:' . $column, $direction]];
		return $this;
	}


	/**
	 * Only load columns with this name
	 * Will NOT call 'with' automatically
	 * Id's and foreign keys will always be loaded
	 * @param string|array $only
	 * @return \Sulfur\Data\Finder
	 */
	public function only($only)
	{
		if(! is_array($only)){
			$only = [$only];
		}
		$this->only = array_unique(array_merge($this->only, $only));

		return $this;
	}


	/**
	 * Eager load a relation by name
	 * Provide a closure with additional SQL for the relation
	 * @param string $name
	 * @param \Closure $sql
	 * @return \Sulfur\Data\Finder
	 */
	public function with($name = null, $sql = null)
	{
		$with = explode('.', $name);
		if(count($with) > 1) {
			// nested.with.deeper given
			// build a nested function from the back to the front
			// sql cannot be used
			$withFunction = null;
			while($name = array_pop($with)) {
				$withFunction = function($relation) use ($name, $withFunction) {
					$relation->with($name, $withFunction);
				};
			}
			$withFunction($this);
		} elseif(isset($this->relations[$name])) {
			// make sure each with has an array
			if(! isset($this->with[$name])) {
				$this->with[$name] = [];
			}
			// add sql to it (dont overwrite)
			if($sql) {
				$this->with[$name][] = $sql;
			}
		}
		return $this;
	}


	/**
	 * Don't load these columns
	 * Will undo calls to with
	 * Id's and foreign keys will always be loaded
	 * @param string|array $without
	 * @return \Sulfur\Data\Finder
	 */
	public function without($without)
	{
		if(! is_array($without)){
			$without = [$without];
		}
		$this->without = array_unique(array_merge($this->without, $without));

		return $this;
	}


	/**
	 * Reset the finder: clear sql, with, without and only
	 */
	public function clear()
	{
		$this->sql = [];
		$this->with = [];
		$this->without = [];
		$this->only = [];
		return $this;
	}


	/**
	 * Load one entity
	 * @param int $id
	 * @return \Sullfur\Data\Entity
	 */
	public function one($id = null)
	{
		if($id !== null) {
			$this->where('id', $id);
		}
		return $this->limit(1)->all()->first();
	}


	/**
	 * Load all wanted entities
	 * @return \Sullfur\Data\Collection
	 */
	public function all($keyOrFlat = false, $value = null)
	{
		// get an entitycollection
		$entities = new Collection($this->entity, $this->query()->iterator(), $this->manager);

		// if this is a 'relation-finder', group the entities by the correct lookup internally
		// so groups can easily be added to the correct parent entity later
		if($this->relation) {
			$entities->map($this->relation->lookup());
		}
		// Eager load child rependencies
		$this->loadWith($entities);

		// return the result
		if($keyOrFlat !== false) {
			if($keyOrFlat === true) {
				return $entities->flat();
			} else {
				return $entities->flat($keyOrFlat, $value);
			}
		} else {
			return $entities;
		}
	}


	/**
	 * Get the count for the current state of the finder
	 * @return int
	 */
	public function count()
	{
		// create a select with a dummy entry for all the rows
		$select = $this->database
		->select($this->database->raw('1 as row'))
		->from($this->table);
		// add sql, including group by's
		foreach($this->sql as $sql) {
			if(! in_array($sql[0], ['select', 'order'])){
				$select->{$sql[0]}(...$sql[1]);
			}
		}
		// create a query to count the result
		return $this->database
		->select($this->database->raw('count(*) as cnt'))
		->from([$select, 'rows'])->result()[0]['cnt'];
	}


	/**
	 * Build the query
	 * @return Query
	 */
	public function query()
	{
		$query = $this->database
		->select($this->table . '.id')
		->from($this->table);

		// start with column names
		$columns = array_keys($this->columns);

		if($this->only){
			// only select these
			$columns = array_intersect($columns, $this->only);
		}if($this->without) {
			// dont select these
			$columns = array_diff($columns, $this->without);
		}

		// always include foreign keys
		foreach($this->columns as $column => $type){
			if(substr($column, -3) === '_id'){
				$columns[] = $column;
			}
		}


		// prefix columns with tablename
		$prefixed = [];
		foreach($columns as $column){
			$prefixed[] = $this->table . '.' . $column;
		}

		// select columns only once
		$query->select(...array_unique($prefixed));


		// add stacked statmements
		foreach($this->sql as $sql) {
			$query->{$sql[0]}(...$sql[1]);
		}
		return $query;
	}


	/**
	 * Go through all the eager loading relations and let those relations
	 * set their entities on the base entities
	 * @param Collection $entities
	 */
	protected function loadWith(Collection $entities)
	{
		if(count($this->with) > 0) {
			// gather all the eager loading relations
			$relations = [];
			// keys to pluck
			$pluck = [];
			foreach($this->with as $name => $sqls){
				if(! in_array($name, $this->without)) {
					// create a new relation
					$relation = new Relation($this->entity, $name, $this->manager);
					// if there is are sql closures, run now on the relation's finder
					foreach($sqls as $sql) {
						$sql($relation->finder());
					}
					// Relations need a flat array of originating ids of fk's to run a IN query on
					// That key is accessed by the 'from' method.
					// Gather what keys to pluck from the entities
					if($relation->from() == 'belongs' && $poly = $relation->poly()) {
						// pluck conditionally for polymorhpic belongs relations
						// the value of the polymorphic column should be equal to the 'related' value of the relation
						$pluckCondition = [$poly => $relation->related()];
					} else {
						// pluck all
						$pluckCondition = [];
					}

					// add to pluck array
					$pluck[$relation->from()] = $pluckCondition;

					// store the relation
					$relations[] = $relation;
				}
			}
			// get the plucked keys
			$keys = $entities->pluck($pluck);
			// Go through all the relations
			foreach($relations as $relation) {
				// let the relation insert its entities, hand it the correct plucked flat set
				$relation->with($entities, $keys[$relation->from()]);
			}
		}
	}
}