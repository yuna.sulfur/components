<?php

namespace Sulfur\Data;

use Sulfur\Data;
use Sulfur\Data\Finder;
use Sulfur\Data\Collection;
use Sulfur\Data\Entity;

class Relation
{
	/**
	 * When lazy loading, this is the origin entity
	 * @var \Sulfur\Data\Entity|null
	 */
	protected $origin;

	/**
	 * Relation name
	 * @var string
	 */
	protected $name;

	/**
	 * Data instance
	 * @var \Sulfur\Data
	 */
	protected $manager;

	/**
	 * Finder instance
	 * @var \Sulfur\Data\Finder
	 */
	protected $finder;

	/**
	 * The class-name of the related entities
	 * @var string
	 */
	protected $entity;

	/**
	 * The type of relation (one,many,belongs,set,junction)
	 * @var string
	 */
	protected $type;

	/**
	 * Database name of the related entities
	 * @var string
	 */
	protected $database;

	/**
	 * Table name of the related entities
	 * @var string
	 */
	protected $table;

	/**
	 * Name of the origin key
	 * @var string
	 */
	protected $from;

	/**
	 * Name of the related key
	 * @var type
	 */
	protected $to;

	/**
	 * Name of the junction table
	 * @var string
	 */
	protected $junction;

	/**
	 * Name of the origin key of the junction table
	 * @var string
	 */
	protected $left;

	/**
	 * Name of the related key of the origin table
	 * @var string
	 */
	protected $right;

	/**
	 * Name of relation type in a polymorhic relation
	 * @var string
	 */
	protected $related;

	/**
	 * Name of column that holds the name of the polymorphic relation
	 * @var string
	 */
	protected $poly = null;

	/**
	 * The name of the key to match against $this->from
	 * @var string
	 */
	protected $lookup = null;

	/**
	 * Additional columns in the junction table
	 * @var array
	 */
	protected $columns = [];


	/**
	 * @param string|\Sulfur\Entity $origin A string for eager loading, an entity for lazy loading
	 * @param string $name Relation name
	 * @param \Sulfur\Data $manager The central data object
	 */
	public function __construct($origin, $name, Data $manager)
	{
		if($origin instanceof Entity){
			// Entity given: we are lazy loading
			$this->origin = $origin;
			// fetch the origin class from the entity
			$origin = get_class($origin);
		}

		$this->name = $name;

		$this->manager = $manager;

		// Extract the relation-params
		$originTable = $origin::table();
		$params = $origin::relation($name);
		$this->entity = $params[0];
		$this->type = $params[1];
		$this->database = call_user_func($this->entity . '::database');
		$this->table = call_user_func($this->entity . '::table');


		/*
		 * (Polymorphic) relations
		 *

		A page has many comments, comment table uses a column indicating the related type and an related id-column.
		'from' indicates the column name of the id-column in the page-table. Defaults to 'id'
		'to' indicates the column name of the page-id in the comment-table . Defaults to 'related_id' for poly and [tablename]_id for non-poly
		'related' indicates the value of the poly-column for a page. If related is not provided, no poly relation is assumed
		'poly' indicates the name of the column used for the poly value. If none is provided is retrieved from the related entity. Defaults to 'related'
		 *
		'Entity\Page'
		'comments' => ['\Comment\Entity', 'many', 'from' => 'id', 'to' => 'comnmented_on_id', 'related' => 'page', 'poly' => 'commented_on' ]



		A comment belongs to a page, comment table uses a column indicating the related type and an related id-column.
		'from' indicates the column name of the page-id in the comment-table . Defaults to 'related_id' for poly and [tablename]_id for non-poly
		'to' indicates the column name of the id-column in the page-table. Defaults to 'id'
		'related' indicates the value of the poly-column for a page. Defaults to the name of the relation
		'poly' indicates the name of the column used for the poly value. If no poly is provided, no poly relation is assumed. true can also be passed. Defaults to 'related'
		 *
		'Entity\Comment'
		'page' => ['Page\Entity', 'belongs', 'from' => 'comnmented_on_id', 'to' => 'id', 'related' => 'page',  'poly' => 'commented_on', ]



		A page has many tags. Tags can belong to many pages. Pages can belong to many tags. No poly available
		'from' indicates the column name of the id-column in tha page table. defaults to 'id'
		'to' indicates the column name of the id-column in the tag-table. Defaults to 'id'
		'junction' indicates the name of the junction table. Defaults to [from_table]_[to_table]
		'left' indicates the column-name holding the page-id in the junction-table. Defaults to [from_table]_id
		'right' indicates the column-name holding the tag-id in the junction-table. Defaults to [to_table]_id
		'columns' defines extra columns in the junction table. Defaults to empty array
		*
		'Entity\Page'
		'tags' => ['Tag\Entity', 'junction', 'from' => 'id', 'to' => 'id', 'junction' => 'page_tag', 'left' => 'page_id', 'right' => 'tag_id', 'columns' => ['tagged_at']]
		 *
		'Entity\Tag'
		 'pages' => ['Page\Entity', 'junction', 'from' => 'id', 'to' => 'id', 'junction' => 'page_tag', 'left' => 'page_id', 'right' => 'tag_id']
		 'articles' => ['Article\Entity', 'junction', 'from' => 'id', 'to' => 'id', 'junction' => 'article_tag', 'left' => 'article_id', 'right' => 'tag_id']



		Page belongs to a set of images
		'from' indicates the column name of the set_id-column in the page table. defaults to [to_table]_set_id
		'to' indicates the column name of the id-column in the image-table. Defaults to 'id'
		'junction' indicates the name of the set table. Defaults to [to_table]_set
		'left' indicates the column-name holding the set-id in the junction-table. Defaults to set_id
		'right' indicates the column-name holding the image-id in the junction-table. Defaults to [to_table]_id
		'columns' defines extra columns in the junction table. Defaults to empty array
		*
		'Entity\Page'
		'images' => ['Image\Entity', 'set', 'from' => 'id', 'to' => 'id', 'junction' => 'image_set', 'left' => 'image_set_id', 'right' => 'image_id']
		 *

		 */




		// figure out polymorphic settings
		switch($this->type){
			case 'one':
			case 'many':
				$this->related = isset($params['related']) ? $params['related'] : null;
				if($this->related) {
					if(isset($params['poly'])) {
						$this->poly = $params['poly'];
					} else {
						$polyRelation = call_user_func($this->entity . '::relation', $this->related);
						$this->poly = isset($polyRelation['poly']) ? $polyRelation['poly'] : 'related';
					}
				}
				break;
			case 'belongs':
				$this->poly = isset($params['poly']) ? (is_string($params['poly']) ? $params['poly'] : 'related') : null;
				if($this->poly) {
					$this->related = isset($params['related']) ? $params['related'] : $name;
				}
				break;
		}


		switch($this->type){
			case 'one':
			case 'many':
				$this->from = isset($params['from']) ? $params['from'] : 'id';
				$this->to = isset($params['to']) ? $params['to'] : ($this->poly ? 'related_id' : $originTable . '_id');
				$this->lookup = $this->to;
				break;
			case 'belongs':
				$this->from = isset($params['from']) ? $params['from'] :($this->poly ? 'related_id' : $this->table . '_id');
				$this->to = isset($params['to']) ? $params['to'] : 'id';
				$this->lookup = $this->to;
				break;
			case 'junction':
				$this->from = isset($params['from']) ? $params['from'] : 'id';
				$this->to = isset($params['to']) ? $params['to'] : 'id';
				$this->junction = isset($params['junction']) ? $params['junction'] : $originTable . '_' . $this->table;
				$this->left = isset($params['left']) ? $params['left'] : $originTable . '_id';
				$this->right = isset($params['right']) ? $params['right'] : $this->table . '_id';
				$this->lookup = 'junction:' . $this->left;
				$this->columns = isset($params['columns'])
					? (is_array($params['columns']) ? $params['columns'] : [$params['columns']])
					: [];
				break;
			case 'set':
				$this->from = isset($params['from']) ? $params['from'] : $this->table . '_set_id';
				$this->to = isset($params['to']) ? $params['to'] : 'id';
				$this->junction = isset($params['junction']) ? $params['junction'] : $this->table . '_set';
				$this->left = isset($params['left']) ? $params['left'] : 'set_id';
				$this->right = isset($params['right']) ? $params['right'] :  $this->table . '_id';
				$this->lookup = 'junction:' . $this->left;
				$this->columns = isset($params['columns'])
					? (is_array($params['columns']) ? $params['columns'] : [$params['columns']])
					: [];
				break;
		}


		// create a finder
		$this->finder = new Finder($this->entity, $manager);

		// set the relation in the finder
		$this->finder->relation($this);

		// A concrete origin entity was given (lazy loading), immediately apply SQL
		if($this->origin) {
			$ids = [ $this->origin->{$this->from} ];
			if($this->poly && $this->type === 'belongs') {
				if($this->origin->{$this->poly} !== $this->related) {
					// the origin of polymorphic relation of type belongs
					// needs the poly column to be of the value {$this->related}
					// if not, dont select anything
					$ids = [];
				}
			}
			$this->applyOriginSQL($ids);
		}
	}


	/**
	 * Get a protected property
	 * @param string $name
	 * @return mixed
	 */
	public function __call($name, array $args)
	{
		if(in_array($name, ['finder', 'entity', 'name', 'type', 'database', 'table', 'from', 'to', 'junction', 'left', 'right', 'lookup', 'columns', 'finder', 'origin', 'related', 'poly'] )){
			return $this->{$name};
		}
	}


	/**
	 * Load the relation entities on the given origin entities
	 * @param array $entities The parent entities
	 * @param array $keys extracted id and foreign keys from the entities to speed up the process
	 */
	public function with(Collection $origins, array $keys, array $poly = null)
	{
		// apply the sql for the entites
		$this->applyOriginSQL($keys);

		// get the related entities
		$entities = $this->finder->all();

		// fill up the entities with groups of this relation
		foreach($origins as $origin){
			// the property with the relationname is filled with the group
			// that matches the entity's parentKey value
			$mapped = $entities->mapped($this->lookup, $origin->{$this->from});
			if ($this->type === 'one' || $this->type === 'belongs') {
				if(isset($mapped[0])) {
					$origin->{$this->name} = $mapped[0];
				} else {
					$origin->{$this->name} = null;
				}
			} else {
				$origin->{$this->name} = $mapped;
			}
		}
	}


	/**
	 * Apply SQL for a flat array of parentKeys
	 * @param array $keys
	 */
	protected function applyOriginSQL(array $keys)
	{
		if(count($keys) === 0){
			// empty keys, select nothing
			$this->finder->where(false);
		} else {
			switch($this->type){
				case 'one':
				case 'many':
				case 'belongs':
					$this->finder
					->where($this->table . '.' . $this->to, 'in', array_unique($keys))
					->select($this->table . '.' . $this->lookup);
					if($this->poly && ($this->type === 'one' || $this->type === 'many')) {
						// No need to apply poly on 'belongs', as the finder does not contain the poly column
						// This is taken care of in the constructor for lazy loading
						// Or in the Data\Finder::loadWith() method for eager loading
						$this->finder->where($this->table . '.' . $this->poly, $this->related);
					}
					break;
				case 'junction':
				case 'set':
					foreach ($this->columns as $column => $properties) {
						$this->finder->select([$this->junction . '.' . $column, 'junction:' . $column]);
					}
					$this->finder->join($this->junction, 'inner')
					->on($this->junction . '.' . $this->right, $this->table . '.' . $this->to)
					->onWhere($this->junction . '.' . $this->left, 'in', array_unique($keys))
					->select([$this->junction . '.' . $this->left , $this->lookup])
					->order($this->junction . '.id', 'ASC');
			}
		}
	}


	/**
	 * Get relation entities by id
	 * @param int|array $relatives
	 * @param array $with Additional eager loaded relations
	 */
	public function get($relatives, array $with = [])
	{
		// normailze the relatives
		$relatives = $this->manager->storage()->relatives($relatives);

		if(count($relatives) === 0){
			// empty ids, select nothing
			$finder = $this->finder->where(false);
		} else {
			// get ids
			$ids = array_column(($relatives), 'id');
			// ecape them
			array_walk($ids, function($id){ return (int) $id; });
			// set query
			$finder = $this->finder
			->where('id', 'in', $ids)
			->order($this->finder->raw('FIELD(id,' . implode(',', $ids) . ' )'), 'ASC');

			foreach($with as $name) {
				$finder->with($name);
			}
		}

		if ($this->type === 'one' || $this->type === 'belongs') {
			// only return one
			return $finder->one();
		} elseif($this->type === 'junction' || $this->type === 'set') {
			// get multiple: there can be multiples and junction values

			// build a dictionary
			$entities = [];
			foreach($finder->all() as $entity) {
				$entities[$entity->id] = $entity;
			}

			// build results from given relatives
			$results = [];
			foreach($relatives as $relative) {
				if(isset($entities[$relative['id']])) {
					// clone an entity and set junction values
					$result = clone($entities[$relative['id']]);
					unset($relative['id']);
					$result->junction($relative);
					$results[] = $result;
				}
			}
			return $results;
		} else {
			// get many, no doubles, no juntion values
			return $finder->all()->flat();
		}
	}


	/**
	 * Set (overwrite) relatives
	 * @param int|array $relatives
	 * @return $this
	 */
	public function set($relatives)
	{
		$this->manager->storage()->set($this, $relatives);
		return $this;
	}


	/**
	 * Add relatives
	 * @param int|array $relatives
	 * @return $this
	 */
	public function add($relatives)
	{
		$this->manager->storage()->add($this, $relatives);
		return $this;
	}


	/**
	 * Remove relatives, pass nothing to remove all relatives
	 * @param int|array|null $relatives
	 * @return $this
	 */
	public function remove($relatives = null)
	{
		$this->manager->storage()->remove($this, $relatives);
		return $this;
	}
}