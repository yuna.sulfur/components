<?php

namespace Sulfur\Data;

use ArrayAccess;
use Sulfur\Data;
use Sulfur\Data\Relation;

class Entity implements ArrayAccess
{
	/**
	 * Database name
	 * @var string
	 */
	protected static $database = null;

	/**
	 * Table name
	 * @var string
	 */
	protected static $table = null;

	/**
	 * Columns
	 * @var array
	 */
	protected static $columns = [];

	/**
	 * Relations
	 * @var array
	 */
	protected static $relations = [];


	/**
	 * Get database name
	 * @return string|null
	 */
	public static function database()
	{
		return static::$database;
	}


	/**
	 * Get table-name
	 * @return string
	 */
	public static function table()
	{
		return static::$table;
	}


	/**
	 * Get columns
	 * @return array
	 */
	public static function columns()
	{
		return static::$columns;
	}


	/**
	 * Get relation by name
	 * @return array|null
	 */
	public static function relation($name)
	{
		if(isset(static::$relations[$name])){
			return static::$relations[$name];
		}
		return null;
	}


	/**
	 * Get or add relations
	 * @return array|null
	 */
	public static function relations(array $relations = null)
	{
		if($relations === null) {
			return static::$relations;
		} else {
			static::$relations = array_merge(static::$relations, $relations);
		}
	}


	/**
	 * Associative array with data
	 * @var array
	 */
	protected $data = [];

	/**
	 * Associative array with dirty values
	 * @var array
	 */
	protected $dirty = [];


	/**
	 * Data object
	 * @var \Sulfur\Data
	 */
	protected $manager;

	/**
	 * Constructor
	 * @param array $data
	 * @param \Sulfur\Data $manager
	 */
	public function __construct(array $data = [], Data $manager = null)
	{
		$this->dirty = $data;
		$this->manager = $manager;
	}


	/**
	 * Set the manager
	 * @param \Sulfur\Data $manager
	 */
	public function manager($manager)
	{
		$this->manager = $manager;
	}


	/**
	 * Get all data as array
	 * Or set data
	 */
	public function data($data = null, $dirty = true)
	{
		if($data === null) {
			$parsed = [];
			// get the original data
			foreach($this->data as $name => $item) {
				$parsed[$name] = $this->__get($name);
			}
			// overwrite with dirty data
			foreach($this->dirty as $name => $item) {
				$parsed[$name] = $item;
			}
			// overwrite with dirty relation data
			foreach(static::$relations as $name => $info) {
				if(isset($this->dirty[$name]) && is_array($this->dirty[$name])) {
					$parsed[$name] = [];
					foreach($this->dirty[$name] as $entity) {
						$parsed[$name][] = $entity->data();
					}
				} elseif(isset($this->dirty[$name]) && $this->dirty[$name] instanceof Entity) {
					$parsed[$name] = $this->dirty[$name]->data();
				}
			}
			$parsed['junction'] = $this->junction();
			return $parsed;
		} elseif(is_array($data)){
			if($dirty) {
				$this->dirty = array_merge($this->dirty, $data);
			} else {
				$this->data = array_merge($this->data, $data);
			}
		}
		return $this;
	}


	/**
	 * Get all the data that was set with data() or __set
	 * @return array
	 */
	public function dirty()
	{
		return $this->dirty;
	}


	/**
	 * Get data from a junction table
	 * @return mixed
	 */
	public function junction($nameOrValues = null, $value = null)
	{
		if(is_array($nameOrValues)){
			// set multiple junction values
			foreach($nameOrValues as $name => $value){
				$this->data['junction:' . $name] = $value;
			}
		} elseif(is_null($nameOrValues)) {
			// get all junction values
			$values = [];
			foreach($this->data as $name => $value){
				if(strpos($name, 'junction:') === 0){
					$values[substr($name, 9)] = $value;
				}
			}
			return $values;
		} elseif(is_null($value)) {
			// get one junction value
			if (isset($this->data['junction:' . $nameOrValues])) {
				return $this->data['junction:' . $nameOrValues];
			}
		} else {
			// set one junction value
			$this->data['junction:' . $nameOrValues] = $value;
		}
	}


	/**
	 * Access a property of an Entity
	 * First we will try to get the property from the data
	 * If that fails, we'll assume it is a relation and let the manager figure it out
	 *
	 * @param string $name
	 * @return int|string|array|null
	 */
	public function __get($name)
	{
		if (array_key_exists($name, $this->dirty)) {
			// dirty vars don't need to be converted
			return $this->dirty[$name];
		} elseif (array_key_exists($name, $this->data)) {
			// db vars need to be converted
			if (isset(static::$columns[$name])) {
				// get column type
				$params = static::$columns[$name];
				if(is_string($params)) {
					$type = $params;
				} elseif(is_array($params)) {
					$type = isset($params[0]) ? $params[0] : (isset($params['type']) ? $params['type'] : null);
				} else {
					$type = null;
				}
				// try to cast it
				switch ($type) {
					case 'int':
					case 'integer':
						return (int) $this->data[$name];
					case 'float':
					case 'decimal':
						return (float) $this->data[$name];
					case 'bool':
					case 'boolean':
						return (bool) $this->data[$name];
					case 'json':
						return json_decode($this->data[$name], true);
					default:
						return $this->data[$name];
				}
			} else {
				// just return the value
				return $this->data[$name];
			}
		} elseif ($this->manager && isset(static::$relations[$name])) {
			// get relation finder
			$finder = $this->__call($name)->finder();
			if(static::$relations[$name][1] == 'one' || static::$relations[$name][1] == 'belongs' ) {
				// return the one related item
				return $finder->one();
			} else {
				// return a finder
				return $finder;
			}
		} else {
			// nothing here
			return null;
		}
	}


	/**
	 * Get a relation object by name, to add or remove entities
	 * @param string $name
	 * @param array $args
	 * @return \Sulfur\Data\Relation
	 */
	public function __call($name, array $args = [])
	{
		if ($this->manager && isset(static::$relations[$name])) {
			return new Relation($this, $name, $this->manager);
		}
	}


	/**
	 * Set a variable
	 * @param string $name
	 * @param mixed $value
	 */
	public function __set($name, $value)
	{
		$this->dirty[$name] = $value;
	}


	/**
	 * Test if a variable is available
	 * @param string $name
	 * @return boolean
	 */
	public function __isset($name)
	{
		return $this->__get($name) !== null;
	}


	/**
	 * Set by array access
	 * @param string $name
	 * @param mixed $value
	 */
	public function offsetSet($name, $value)
	{
		$this->dirty[$name] = $value;
	}


	/**
	 * Get by array access
	 * @param string $name
	 * @return mixed
	 */
	public function offsetGet($name)
	{
		return $this->__get($name);
	}


	/**
	 * Check existence by array access
	 * @param string $name
	 * @return boolean
	 */
	public function offsetExists($name)
	{
		return array_key_exists($name, $this->dirty)
		|| array_key_exists($name, $this->data)
		|| ($this->manager && isset(static::$relations[$name]));
	}


	/**
	 * unset data by array access
	 * @param string $name
	 * @return void
	 */
	public function offsetUnset($name)
	{
		if(array_key_exists($name, $this->dirty)){
			unset($this->dirty[$name]);
		}
		if(array_key_exists($name, $this->data)){
			unset($this->data[$name]);
		}
	}


	/**
	 * Control the debug info
	 * @return array
	 */
	public function __debugInfo()
	{
		return [
			'data' => $this->data,
			'dirty' => $this->dirty
		];
	}


	/**
	 * Control the debug info
	 * @return string
	 */
	public function __toString()
	{
		return var_export($this->__debugInfo(), true);
	}
}