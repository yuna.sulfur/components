<?php

namespace Sulfur;

use Sulfur\Filesystem\File;

use League\Flysystem\Filesystem as Flysystem;
use League\Flysystem\PluginInterface;

use League\Flysystem\Adapter\Local;

class Filesystem
{
	const STORAGE_LOCAL = 'local';

	/**
	 * The Flysystem instance
	 * @var League\Flysystem\Filesystem
	 */
	protected $flysystem;

	/**
	 * Config
	 * @var array
	 */
	protected $config = [
		'storage' => 'local',
		'root' => '/',
	];


	public function __construct(array $config = [])
	{
		$this->config = array_merge($this->config, $config);

		switch($this->config['storage']) {
			case self::STORAGE_LOCAL:
				$adapter = new Local($this->config['root']);
				break;
		}
		$this->flysystem = new Flysystem($adapter);
	}


    public function has($path)
	{
		return $this->flysystem->has($path);
	}


    public function read($path)
	{
		return $this->flysystem->read($path);
	}


    public function readStream($path)
	{
		return $this->flysystem->readStream($path);
	}


    public function contents($directory = '', $recursive = false)
	{
		return $this->flysystem->listContents($directory, $recursive);
	}


    public function metadata($path)
	{
		return $this->flysystem->getMetadata($path);
	}


    public function size($path)
	{
		return $this->flysystem->getSize($path);
	}


    public function mimetype($path)
	{
		return $this->flysystem->getMimetype($path);
	}


    public function timestamp($path)
	{
		return $this->flysystem->getTimestamp($path);
	}


    public function visibility($path)
	{
		return $this->flysystem->getVisibility($path);
	}


    public function write($path, $contents, array $config = [])
	{
		return $this->flysystem->write($path, $contents, $config);
	}


    public function writeStream($path, $resource, array $config = [])
	{
		return $this->flysystem->writeStream($path, $resource, $config);
	}


    public function update($path, $contents, array $config = [])
	{
		return $this->flysystem->update($path, $contents, $config);
	}


    public function updateStream($path, $resource, array $config = [])
	{
		return $this->flysystem->updateStream($path, $resource, $config);
	}


    public function rename($path, $newpath)
	{
		return $this->flysystem->rename($path, $newpath);
	}


    public function copy($path, $newpath)
	{
		return $this->flysystem->copy($path, $newpath);
	}


    public function delete($path)
	{
		return $this->flysystem->delete($path);
	}


    public function deleteDir($dirname)
	{
		return $this->flysystem->deleteDir($dirname);
	}


    public function createDir($dirname, array $config = [])
	{
		return $this->flysystem->createDir($dirname, $config);
	}


    public function setVisibility($path, $visibility)
	{
		return $this->flysystem->setVisibility($path, $visibility);
	}


    public function put($path, $contents, array $config = [])
	{
		return $this->flysystem->put($path, $contents, $config);
	}


    public function putStream($path, $resource, array $config = [])
	{
		return $this->flysystem->putStream($path, $resource, $config);
	}


    public function readAndDelete($path)
	{
		return $this->flysystem->readAndDelete($path);
	}


    public function get($path)
	{
		return new File( $this->flysystem->get($path));
	}


    public function addPlugin(PluginInterface $plugin)
	{
		return $this->flysystem->addPlugin($plugin);
	}
}