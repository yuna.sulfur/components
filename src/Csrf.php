<?php

namespace Sulfur;

use Sulfur\Session;

class Csrf
{
	protected $session = null;

	protected $config = [
		'sessionkey' => 'csrf',
	];

	public function __construct(Session $session, $config = [])
	{
		$this->session = $session;
		$this->config = array_merge($this->config, $config);
	}


	public function token($regenerate = false)
	{
		$token = $this->session->get($this->config['sessionkey'], false);

		if(!is_string($token) || !$token || $regenerate){
			$token = substr(str_replace(['/', '+', '='], '', base64_encode(openssl_random_pseudo_bytes(40))), 0, 20);
			$this->session->set($this->config['sessionkey'], $token);
		}
		return $token;
	}


	public function validate($token)
	{
		$stored = $this->session->get($this->config['sessionkey'], '__none__');

		if(is_string($token) && $token != '' && is_string($stored) && $stored != '' && $token === $stored){
			return true;
		}

		return false;
	}


	public function destroy()
	{
		$this->session->set($this->config['sessionkey'], null);
	}


	public function keepalive()
	{
		$this->session->touch();
	}

}