<?php

namespace Sulfur;

use Symfony\Component\HttpFoundation\Session\Session as Manager;
use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\NativeFileSessionHandler;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\PdoSessionHandler;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\RedisSessionHandler;

use Redis;

class Session
{
	const STORAGE_DATABASE = 'database';
	const STORAGE_REDIS = 'redis';
	const STORAGE_NATIVE = 'native';

	/**
	 * The session manager
	 * @var Symfony\Component\HttpFoundation\Session\Session
	 */
	protected $session;


	protected $config = [
		'name' => 'session',
		'http' => true,
		'secure' => false,
		'lifetime' => 0,
		'path' => '/',
		'domain' => '',
		'storage' => [
			'type' => 'native',
		]
	];


	public function __construct(array $config = [])
	{
		$this->config = array_merge($this->config, $config);

		$options = [
			'name' => $this->config['name'],
			'use_cookies' => true,
			'cookie_httponly' => $this->config['http'],
			'cookie_lifetime' => $this->config['lifetime'],
			'cookie_path' => $this->config['path'],
			'cookie_secure' => $this->config['secure'],
			'cookie_domain' =>  $this->config['domain'],
		];


		if($this->config['storage']['type'] === self::STORAGE_DATABASE){
			$config = $this->config['storage']['database'];
			$handler = new PdoSessionHandler($config['dsn'],[
				'db_table' => 'session',
				'db_id_col' => 'id',
				'db_data_col' => 'data',
				'db_lifetime_col' => 'lifetime',
				'db_time_col' => 'modified',
				'db_username' => $config['username'],
				'db_password' => $config['password'],
				'db_connection_options' => $config['options'],
			]);
		} elseif($this->config['storage']['type'] === self::STORAGE_REDIS){
			$config = $this->config['storage']['redis'];
			$redis = new Redis();
			$redis->connect($config['host'], (int) $config['port']);
			if($config['auth']) {
				$redis->auth($config['auth']);
			}
			$handler = new RedisSessionHandler($redis);
		} else {
			$handler = new NativeFileSessionHandler();
		}

		$this->session = new Manager(new NativeSessionStorage($options, $handler));

		// custom session name
		$this->session->setName($this->config['name']);

		// start the session
		$this->session->start();
	}


	public function set($name, $value)
	{
		$this->session->set($name, $value);
	}


	public function get($name, $default = null)
	{
		return $this->session->get($name, $default);
	}


	public function destroy()
	{
		$this->session->invalidate();
	}


	public function regenerate()
	{
		return $this->session->migrate();
	}


	public function touch()
	{
		$this->session->has('__touched__');
	}
}