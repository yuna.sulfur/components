<?php

namespace Sulfur;

use Swift_SmtpTransport;
use Swift_SendmailTransport;
use Swift_Mailer;
use Swift_Message;
use Swift_Attachment;

class Email
{
	const TRANSPORT_SENDMAIL = 'sendmail';
	const TRANSPORT_SMTP = 'smtp';

	protected $config = [
		'transport' => 'sendmail',
		'command' => '/usr/sbin/sendmail -bs',
		'host' => '',
		'port' => 25,
		'username' => '',
		'password' => '',
		'encryption' => null,
	];

	protected $mailer = null;


	public function __construct(array $config = [])
	{
		$this->config = array_merge($this->config, $config);
	}

	protected function mailer()
	{
		if($this->mailer === null) {
			switch($this->config['transport']) {
				case self::TRANSPORT_SMTP:
					$transport = (new Swift_SmtpTransport($this->config['host'], $this->config['port'], $this->config['encryption']))
					->setUsername($this->config['username'])
					->setPassword($this->config['password']);
					break;
				default:
					$transport = new Swift_SendmailTransport($this->config['command']);
					break;
			}
			$this->mailer = new Swift_Mailer($transport);
		}

		return $this->mailer;
	}



	public function send($message)
	{
		$mailer = $this->mailer();

		$swiftMessage = (new Swift_Message($message->subject()))
		->setFrom($message->from())
		->setTo($message->to())
		->setCc($message->cc())
		->setBcc($message->bcc());

		$body = $message->body();

		if(strip_tags($body) == $body) {
			$swiftMessage->setBody($body);
		} else {
			$swiftMessage
			->setBody(str_replace(['</p>', '<br />', '<br>'], "\n", $body))
			->addPart($body, 'text/html');
		}

		foreach($message->attachments() as $file => $name) {
			if(is_numeric($file)) {
				$message->attach(Swift_Attachment::fromPath($name));
			} else {
				$message->attach(Swift_Attachment::fromPath($file)->setFilename($name));
			}
		}

		return $this->mailer()->send($swiftMessage);
	}



	public function message($subject, $to, $from, $body = '', $attachments = [], $cc = [], $bcc = [])
	{
		return new Class($this, $subject, $to, $from, $body, $attachments, $cc, $bcc) {

			protected $mailer;
			protected $subject;
			protected $from = [];
			protected $to = [];
			protected $body;
			protected $attachments;
			protected $cc;
			protected $bcc;

			public function __construct($mailer, $subject, $to, $from, $body, $attachments, $cc, $bcc)
			{
				$this->mailer = $mailer;
				$this->subject = $subject;
				$this->from = is_array($from) ? $from : [$from => $from];
				$this->to = is_array($to) ? $to : [$to];

				$this->body = $body;
				$this->attachments = $attachments;
				$this->cc = is_array($cc) ? $cc : [$cc];
				$this->bcc = is_array($bcc) ? $bcc : [$bcc];
			}

			public function send() {
				$this->mailer->send($this);
			}


			public function subject($subject = null) {
				if($subject === null) {
					return $this->subject;
				} else {
					$this->subject = $subject;
				}
			}

			public function from($email = null, $name = null) {
				if($email === null) {
					return $this->from;
				} else {
					$this->from = $name === null ? [$email => $email] : [$email => $name];
				}
			}

			public function to($email = null, $name = null) {
				if($email === null) {
					return $this->to;
				} else {
					if($name === null) {
						$this->to[] = $email;
					} else {
						$this->to[$email] = $name;
					}
				}
			}

			public function body($body = null) {
				if($body === null) {
					return $this->body;
				} else {
					$this->body = $body;
				}
			}

			public function attachments($attachments = null) {
				if($attachments === null) {
					return $this->attachments;
				} else {
					$this->attachments = array_merge($this->attachments, $attachments);
				}
			}

			public function cc($email = null, $name = null) {
				if($email === null) {
					return $this->cc;
				} else {
					if($name === null) {
						$this->cc[] = $email;
					} else {
						$this->cc[$email] = $name;
					}
				}
			}

			public function bcc($email = null, $name = null) {
				if($email === null) {
					return $this->bcc;
				} else {
					if($name === null) {
						$this->bcc[] = $email;
					} else {
						$this->bcc[$email] = $name;
					}
				}
			}
		};
	}
}