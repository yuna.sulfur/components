<?php

namespace Sulfur;

use Sulfur\Cache\Storage;

class Cache
{
	const STORAGE_FILE = 'file';
	const STORAGE_APC = 'apc';
	const STORAGE_MEMCACHE = 'memcache';
	const STORAGE_MEMCACHED = 'memcached';
	const STORAGE_REDIS = 'redis';

	protected $storage;

	public function __construct(Storage $storage)
	{
		$this->storage = $storage;
	}


	public function set($name, $value, $lifetime = 0)
	{
		$this->storage->write($name, $value, $lifetime);
	}


	public function get($name, $default = null)
	{
		return $this->storage->read($name, $default);
	}


	public function delete($name)
	{
		$this->storage->delete($name);
	}
}