<?php

namespace Sulfur;

use Sulfur\Filesystem;
use Intervention\Image\ImageManager;
use Exception;

class Image
{

	const DRIVER_GD = 'gd';
	const DRIVER_IMAGICK = 'imagick';

	protected $origin;
	protected $cache;

	/**
	 * Config
	 * @var array
	 */
	protected $config = [
		'driver' => 'gd',
		'filesystem' => [
			'origin' => '',
			'cache' => '',
		],
		'presets' => [],
		'cache' => true
	];


	/**
	 * Intervention
	 * @var Intervention\Image\ImageManager
	 */
	protected $manager;


	public function __construct(
		Filesystem $origin,
		Filesystem $cache,
		array $config = []
	)
	{
		$this->origin = $origin;
		$this->cache = $cache;
		$this->config = array_merge($this->config, $config);
		$this->manager = new ImageManager(['driver' => $this->config['driver']]);
	}


	public function image($source)
	{
		$image = false;
		try{
			if($this->origin->has($source)) {
				$image = $this->manager->make($this->origin->read($source));
			} else {
				$image = $this->manager->make($source);
			}
		} catch(Exception $e) {

		}
		return $image;
	}


	public function save($image, $destination)
	{
		$this->origin->put($destination, $image->encode());
	}


	public function preset($source, $preset)
	{
		$cache = $this->cachePath($source, $preset);

		if($this->config['cache'] && $this->cache->has($cache)) {
			$image = $this->manager->make($this->cache->read($cache));
		} else {
			$image = $this->image($source);

			// check the file
			if(! $image) {
				throw new Exception('unable to create image from ' . $source);
			}

			// check the preset
			if(! isset($this->config['presets'][$preset])) {
				throw new Exception('Unknown image preset: ' . $preset, 404);
			}

			$preset = $this->config['presets'][$preset];
			$width = isset($preset['width']) ? $preset['width'] : null;
			$height = isset($preset['height']) ? $preset['height'] : null;
			$enlarge = isset($preset['enlarge']) ? $preset['enlarge'] : true;
			$quality = isset($preset['quality']) ? $preset['quality'] : 90;
			$stretch = isset($preset['stretch']) ? $preset['stretch'] : false;
			$position = isset($preset['position']) ? $preset['position'] : null;
/*
top-left
top
top-right
left
center (default)
right
bottom-left
bottom
bottom-right
*/
			if(isset($preset['crop']) && $preset['crop']) {
				$image->fit($width, $height, function ($constraint) use ($enlarge) {
					if(! $enlarge) {
						$constraint->upsize();
					}
				}, $position);
			} else {
				$image->resize($width, $height, function ($constraint) use ($enlarge, $stretch) {
					if(! $enlarge) {
						$constraint->upsize();
					}
					if(! $stretch) {
						 $constraint->aspectRatio();
					}
				});
			}

			// create cache file
			if($this->config['cache']) {
				$this->cache->put($cache, $image->encode(null, $quality));
			}
		}
		return $image;
	}



	public function delete($source)
	{
		if($this->origin->has($source)) {
			$this->origin->delete($source);
		}
		if($this->config['cache']) {
			foreach($this->config['presets'] as $preset => $info) {
				$cache = $this->cachePath($source, $preset);
				if($this->cache->has($cache)) {
					$this->cache->delete($cache);
				}
			}
		}
	}



	public function serve($source, $preset = null)
	{
		if($preset) {
			$image = $this->preset($source, $preset);
		} else {
			$image = $this->image($source);
		}
		$data = (string) $image->encode();

		// output image
		header('Content-Type:' . $image->mime());
		header('Content-Length:' . strlen($data));
		header('Cache-Control:max-age=31536000, public');
		header('Expires:' . date_create('+1 years')->format('D, d M Y H:i:s').' GMT');
		echo $data;
		exit;
	}


	protected function cachePath($source, $preset)
	{
		return $preset . DIRECTORY_SEPARATOR . $source;
	}


	protected function extension($mime)
	{
		switch (strtolower($mime)) {
            case 'gif':
            case 'image/gif':
               return 'gif';
            case 'png':
            case 'image/png':
            case 'image/x-png':
               return 'png';
            case 'jpg':
            case 'jpeg':
            case 'image/jpg':
            case 'image/jpeg':
            case 'image/pjpeg':
               return 'jpg';
            case 'tif':
            case 'tiff':
            case 'image/tiff':
            case 'image/tif':
            case 'image/x-tif':
            case 'image/x-tiff':
                return 'tif';
            case 'bmp':
            case 'image/bmp':
            case 'image/ms-bmp':
            case 'image/x-bitmap':
            case 'image/x-bmp':
            case 'image/x-ms-bmp':
            case 'image/x-win-bitmap':
            case 'image/x-windows-bmp':
            case 'image/x-xbitmap':
               return 'bmp';
            case 'ico':
            case 'image/x-ico':
            case 'image/x-icon':
            case 'image/vnd.microsoft.icon':
               return 'ico';
            case 'psd':
            case 'image/vnd.adobe.photoshop':
                return 'ico';
        }
	}
}