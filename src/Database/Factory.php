<?php

namespace Sulfur\Database;

use Sulfur\Database;
use Sulfur\Database\Connection;
use Sulfur\Database\Mysql;
use Sulfur\Database\Postgresql;
use Sulfur\Database\Sqlite;

class Factory
{
	protected static $connections = [];

	protected static $pdo = null;


	public static function pdo($pdo)
	{
		static::$pdo = $pdo;
	}


	public static function make(array $config, $name = null)
	{
		if($name === null) {
			$name = key($config);
		}
		$config = $config[$name];

		if(!isset(self::$connections[$name])) {
			switch ($config['type']) {
				case Database::TYPE_MYSQL:
					$connection = new Mysql($config, static::$pdo);
					break;
				case Database::TYPE_POSTGRESQL:
					$connection = new Postgresql($config, static::$pdo);
					break;
				case Database::TYPE_SQLITE:
					$connection = new Sqlite($config, static::$pdo);
					break;
				default:
					throw new Exception('Unknown database-type: ' . $config['type'] );
			}
			self::$connections[$name] = $connection;
		}
		return new Database(self::$connections[$name]);
	}
}