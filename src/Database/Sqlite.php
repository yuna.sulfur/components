<?php

namespace Sulfur\Database;

class Sqlite extends Connection
{
	/**
	 * The quote style that should be used
	 * @var string 
	 */
	protected $quote = '"';
}
