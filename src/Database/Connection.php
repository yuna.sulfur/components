<?php

namespace Sulfur\Database;

use PDO;
use Exception;

class Connection
{
	/**
	 * The default quote that should be used
	 * @var string
	 */
	protected $quote = '`';


	/**
	 * The actual connection
	 * @var PDO
	 */
	protected $pdo;


	/**
	 * Connection params
	 * @var array
	 */
	protected $params = [
		'dsn' => '',
		'username' => '',
		'password' => '',
		'options' => [],
		'log' => false
	];


	/**
	 * Last query executed
	 * @var array
	 */
	protected $last = [
		'query' => null,
		'params' => []
	];


	/**
	 * All queries executed
	 * @var array
	 */
	protected $all = [];


	/**
	 * Construct
	 * @param array $params
	 * @param \Sulfur\Profile $profile
	 */
	public function __construct(array $params = [], $pdo = null)
	{
		$this->params = array_merge($this->params, $params);
		$this->pdo = $pdo;
	}


	/**
	 * Connect to DB and store connection
	 */
	public function connect()
	{
		if ($this->pdo === null) {
			$this->pdo = new PDO(
				$this->params['dsn'], $this->params['username'], $this->params['password'], $this->params['options']
			);
		}
	}


	/**
	 * Get the quote character
	 * Used by Query
	 * @return string
	 */
	public function quote()
	{
		return $this->quote;
	}


	/**
	 * Run a query on the connection
	 * @param string $query
	 * @param array $params
	 * @param string $type select / insert update / delete
	 * @param string $id the name of the id column for lastInsertId
	 * @return mixed resultset, rowcount of last inserted id
	 * @throws Exception
	 */
	public function execute($query, array $params, $type = null, $id = 'id')
	{
		$this->connect();

		$statement = $this->pdo->prepare($query);

		$start = microtime(true);
		$result = $statement->execute($params);

		$this->last = [
			'query' => $query,
			'params' => $params,
			'time' => microtime(true) - $start
		];

		if($this->params['log']) {
			$this->all[] = $this->last;
		}

		if ($result == false) {
			$error = $statement->errorInfo();
			throw new Exception($error[2]);
		}

		if ($type === 'select' || ($type === 'query' && strtoupper(substr($query, 0, 6)) === 'SELECT')) {
			return $statement;
		} elseif ($type === 'insert' || ($type === 'query' && strtoupper(substr($query, 0, 6)) === 'INSERT')) {
			return $this->pdo->lastInsertId($id);
		} else {
			return $statement->rowCount();
		}
	}


	/**
	 * Return the last executed query and params for debugging purposes
	 * @return array
	 */
	public function last()
	{
		return $this->last;
	}


	/**
	 * Return all executed queries and params for debugging purposes
	 * @return array
	 */
	public function all()
	{
		return $this->all;
	}
}