<?php

namespace Sulfur;

class Dom
{
	protected $tag;

	protected $attributes = [];

	protected $children = [];

	protected $text = '';


	public function __construct($tag, $attributes = [], $text = '')
	{
		$this->tag = $tag;
		$this->attributes = $attributes;
		$this->text = $text;
	}


	public function append($dom)
	{
		array_push($this->children, $dom);
		return $this;
	}


	public function prepend($dom)
	{
		array_unshift($this->children, $dom);
		return $this;
	}


	/**
	 * Dom::attribute()
	 * add an attribute to the dom element
	 * append instead of overwrite with append = true
	 *
	 * @param string $var
	 * @param string $val
	 * @param bool $append
	 * @param string $separator
	 * @return void
	 */
	public function attribute($var, $val, $append = false, $separator = ' ')
	{
		if($append){
			if(isset($this->attributes[$var])){
				$this->attributes[$var] = $this->attributes[$var] . $separator . $val;
			} else {
				$this->attributes[$var] = $val;
			}
		} else {
			$this->attributes[$var] = $val;
		}
		return $this;
	}


	/**
	 * Dom::anchor()
	 * set the spot in ht html that should be replaced by children
	 *
	 * @param string $anchor
	 * @return void
	 */
	public function text($text, $append = false)
	{
		if($append){
			$this->text .= $text;
		} else {
			$this->text = $text;
		}
		return $this;
	}



	/**
	 * Get first child
	 * @return array
	 */
	public function firstChild()
	{
		return $this->child(0);
	}


	/**
	 * Get last child
	 * @return array
	 */
	public function lastChild()
	{
		return $this->child(count($this->children)-1);
	}


	/**
	 * Dom::child()
	 * get child by index
	 *
	 * @param mixed $index
	 * @return
	 */
	public function child($index)
	{
		if(isset($this->children[$index])){
			return $this->children[$index];
		} else {
			return null;
		}
	}


	/**
	 * Dom::render()
	 * output html
	 *
	 * @return string
	 */
	public function render()
	{

		$tag = strtolower($this->tag);
		$text =  $this->text;
		$attributes = '';

		foreach($this->attributes as $name => $value) {
			$attributes .= $name . '="' . htmlentities($value, ENT_QUOTES). '" ';
		}

		// open tag
		$html = '<' . $tag. ' ' . $attributes . '>' . PHP_EOL . $text . PHP_EOL;

		// children
		foreach($this->children as $child){
			$html .= $child->render() . PHP_EOL;
		}

		// close tag
		$html .= '</' . $tag . '>' . PHP_EOL;
		return $html;
	}
}
