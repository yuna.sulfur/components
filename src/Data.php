<?php

namespace Sulfur;

use Sulfur\Data\Finder;
use Sulfur\Data\Relation;
use Sulfur\Data\Entity;
use Sulfur\Data\Storage;

class Data
{
	/**
	 * Create a Data object
	 * Pass in one database or an array of database under named keys
	 * @param Sulfur\Database|array $databases
	 */
	public function __construct($databases = [])
	{
		if(! is_array($databases)) {
			$databases = [$database];
		}
		$this->databases = $databases;
	}


	/**
	 * Get a database
	 * @param string $name
	 * @return Sulfur\Database
	 */
	public function database($name = null)
	{
		if($name == null){
			reset($this->databases);
			$name = key($this->databases);
		}
		return $this->databases[$name];
	}


	/**
	 * Shortcut for ::finder
	 * @param string $class
	 * @return Sulfur\Finder
	 */
	public function __invoke($class)
	{
		return $this->finder($class);
	}


	/**
	 * Get a finder for an entity class
	 * @param string $class Entity class-name
	 * @return \Sulfur\Finder
	 */
	public function finder($class)
	{
		return new Finder($class, $this);
	}


	/**
	 * Alias for finder()
	 * @param string $class Entity class-name
	 * @return \Sulfur\Finder
	 */
	public function get($class)
	{
		return $this->finder($class);
	}


	/**
	 * Get a storage object
	 * @return \Sulfur\Storage
	 */
	public function storage()
	{
		return new Storage($this);
	}


	/**
	 * Get a hydrated entity from data
	 * @param string $class
	 * @param array $data
	 * @param array $with Extra eager loaded
	 * @return \Sulfur\Data\Entity
	 */
	public function hydrate($class, array $data = [], array $with = [])
	{
		// get the relations separately
		$relationData = [];
		$relations = [];

		foreach($class::relations() as $name => $info) {
			if(isset($data[$name]) && $data[$name]) {
				// $data holds the contents of a relation
				// set as relationData, remove it from data
				$relationData[$name] = $data[$name];
				unset($data[$name]);
			} else {
				// mark it as to load
				$relations[] = $name;
			}
		}

		// create entity with dirty data
		$entity = new $class($data, $this);

		// create a usable 'with' map
		// withs are provided as 'with1.with2.with3'  The first with is the relation name, so we should remove that
		// and group by that
		$withMap = [];
		foreach($with as $w) {
			$parts = explode('.' , $w);
			$key = array_shift($parts);
			if(! isset($withMap[$key])) {
				$withMap[$key] = [];
			}

			if(count($parts) > 0) {
				$withMap[$key][] = implode('.' , $parts);
			}
		}

		// load given relatives by id and set them
		foreach($relationData as $name => $data){
			$entity->{$name} = (new Relation($class, $name, $this))->get($data, isset($withMap[$name]) ? $withMap[$name] : []);
		}

		// for all missing relations, load from the original entity data
		// but only if it was asked for in $with
		foreach($relations as $name) {
			if(isset($withMap[$name])) {
				$relation =  $entity->__call($name);
				$finder = $relation->finder();
				foreach($withMap[$name] as $with) {
					$finder->with($with);
				}
				if($relation->type() === 'one' || $relation->type() === 'belongs') {
					$entity->{$name} = $finder->one();
				} else {
					$entity->{$name} = $finder->all();
				}
			}
		}
		return $entity;
	}


	/**
	 * Create a new entity
	 * @param type $class
	 * @param array $data
	 * @param array $relations
	 * @return type
	 */
	public function create($class, array $data = [], array $relations = [])
	{
		// unset id
		unset($data['id']);
		// hydrate an enttity
		$entity = $this->hydrate($class, $data, $relations);
		// save it
		$this->save($entity, $relations);
		// done
		return $entity;
	}


	/**
	 * Persist an entity and relations
	 * @param \Sulfur\Data\Entity $entity
	 * @param array $relations
	 */
	public function save(Entity $entity, array $relations = [])
	{
		// make sure the entity has a manager
		$entity->manager($this);
		// save it
		$this->storage()->save($entity, $relations);
	}


	/**
	 * Delete an entity and its junction tables
	 * @param \Sulfur\Data\Entity $entity
	 */
	public function delete($entity)
	{
		// make sure the entity has a manager
		$entity->manager($this);
		// delete it
		$this->storage()->delete($entity);
	}
}
