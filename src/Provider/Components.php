<?php

namespace Sulfur\Provider;

class Components
{
	public static function register($container)
	{
		$config = $container->get('Sulfur\Config');

		$container->set([
			'Sulfur\Cache' => [
				'Sulfur\Cache\Factory::make',
				'config' => [$config, ':resource' => 'cache'],
			],
			'Sulfur\Database' => [
				'Sulfur\Database\Factory::make',
				'config' => [$config, ':resource' => 'database'],
			],
			'Sulfur\Data' => function() use ($container, $config){
				$databases = [];
				foreach($config('database') as $name => $config) {
					$databases[$name] = $container->get('Sulfur\Database', [':name'=> $name]);
				}
				return new \Sulfur\Data($databases);
			},
			'Sulfur\Email' => [
				'config' => [$config, ':resource' => 'email'],
			],
			'Sulfur\Filesystem' => [
				'Sulfur\Filesystem\Factory::make',
				'config' => [$config, ':resource' => 'filesystem'],
			],
			'Sulfur\Image' => [
				'origin' => ['Sulfur\Filesystem', 'name'=> [$config, ':resource' => 'image', ':key' => 'filesystem.origin', ':default' => 'default' ]],
				'cache' => ['Sulfur\Filesystem', 'name'=> [$config, ':resource' => 'image', ':key' => 'filesystem.cache', ':default' => 'default' ]],
				'config' => [$config, ':resource' => 'image'],
			],
			'Sulfur\Session' => ['config' => [$config, ':resource' => 'session']],
			'Sulfur\Upload' => [':files' => $_FILES],
			'Sulfur\View' => ['config' => [$config, ':resource' => 'view']],
		]);


		$container->share([
			'Sulfur\Form',
			'Sulfur\Session',
			'Sulfur\View',
		]);
	}
}