<?php

namespace Sulfur;

use Sulfur\Database\Connection;
use Sulfur\Database\Query;
use Sulfur\Database\Raw;

class Database
{
	const TYPE_MYSQL = 'mysql';
	const TYPE_POSTGRESQL = 'postgresql';
	const TYPE_SQLITE = 'sqlite';

	/**
	 * The connection
	 * @var \Sulfur\Database\Connection
	 */
	protected $connection;

	/**
	 * Constructor
	 * @param \Sulfur\Database\Connection $connection
	 */
	public function __construct(Connection $connection)
	{
		$this->connection = $connection;
	}


	/**
	 * Create a custom query with ? placeholders and params
	 * @param string $query
	 * @param array $params
	 * @return \Sulfur\Database\Query
	 */
	public function query($query, array $params = [])
	{
		return (new Query($this->connection, 'query'))->query($query, $params);
	}


	/**
	 * Create an insert query
	 * @param string $table
	 * @return \Sulfur\Database\Query
	 */
	public function insert($table)
	{
		return (new Query($this->connection, 'insert'))->table($table);
	}


	/**
	 * Create a select query, pass in select fields as arguments
	 * Pass ['field','alias'] to select a field as alias
	 * @return \Sulfur\Database\Query
	 */
	public function select(...$fields)
	{
		return (new Query($this->connection, 'select'))->select(...$fields);
	}


	/**
	 * Create an update query
	 * @param type $table
	 * @return \Sulfur\Database\Query
	 */
	public function update($table)
	{
		return (new Query($this->connection, 'update'))->table($table);
	}


	/**
	 * Create a delete query
	 * @param string $table
	 * @return \Sulfur\Database\Query
	 */
	public function delete($table)
	{
		return (new Query($this->connection, 'delete'))->table($table);
	}


	/**
	 * Create a Raw object that wont be escaped
	 * @param string $expression
	 * @return \Sulfur\Database\Raw
	 */
	public function raw($expression)
	{
		return new Raw($expression);
	}


	/**
	 * Return the last executed query / params on the connection
	 * @return array
	 */
	public function last()
	{
		return $this->connection->last();
	}


	/**
	 * Return all executed queries and params for debugging purposes
	 * @return array
	 */
	public function all()
	{
		return $this->connection->all();
	}
}