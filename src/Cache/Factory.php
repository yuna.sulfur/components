<?php

namespace Sulfur\Cache;

use Sulfur\Cache;

class Factory
{

	protected static $caches = [];

	public static function make(array $config, $name = null)
	{
		if($name === null) {
			$name = key($config);
		}
		$config = $config[$name];

		if(!isset(self::$caches[$name])) {
			self::$caches[$name] = new Cache(new Storage($config, $name));
		}
		return self::$caches[$name];
	}
}