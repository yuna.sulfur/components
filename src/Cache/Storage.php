<?php

namespace Sulfur\Cache;

use Sulfur\Cache;

use Doctrine\Common\Cache\ApcCache;
use Doctrine\Common\Cache\MemcacheCache;
use Doctrine\Common\Cache\RedisCache;
use Doctrine\Common\Cache\FilesystemCache;
use Memcache;
use Redis;

class Storage
{
	protected $config = [
		'storage' => [
			'type' => '',
			'host' => '',
			'port' => '',
			'path' => '',
			'auth' => false
		],
		'active' => true,
		'lifetime' => 0
	];

	protected $group;

	protected $storage;


	public function __construct(array $config = [], $group = null)
	{
		$this->config = array_replace_recursive($this->config, $config);
		$this->group = $group;

		switch($this->config['storage']['type']) {
			case Cache::STORAGE_FILE:
				$this->storage = new FilesystemCache($this->config['storage']['path']);
				break;
			case Cache::STORAGE_APC:
				$this->storage = new ApcCache();
				break;
			case Cache::STORAGE_REDIS:
				$redis = new Redis();
				$redis->connect($this->config['storage']['host'], (int) $this->config['storage']['port']);
				if($this->config['storage']['auth']) {
					$redis->auth($this->config['storage']['auth']);
				}
				$this->storage = new RedisCache();
				$this->storage->setRedis($redis);
				break;
			case Cache::STORAGE_MEMCACHE:
				$memcache = new Memcache();
				$memcache->connect($this->config['storage']['host'], (int) $this->config['storage']['port']);
				$this->storage = new MemcacheCache();
				$this->storage->setMemcache($memcache);
				break;
			case Cache::STORAGE_MEMCACHED:
				$memcached = new Memcached();
				$memcached->addServer($this->config['storage']['host'], (int) $this->config['storage']['port']);
				$this->storage = new MemcacheCache();
				$this->storage->setMemcached($memcached);
				break;
		}
	}


	public function read($key, $default = null)
	{
		if($this->config['active']) {
			$key = $this->key($key);
			if($this->storage->contains($key)) {
				return $this->storage->fetch($key);
			}
		}
		return $default;
	}


	public function write($key, $value, $lifetime = null)
	{
		if($this->config['active']) {
			$key = $this->key($key);
			$lifetime = $lifetime === null ? $this->config['lifetime'] : $lifetime;
			$this->storage->save($key, $value, $lifetime);
		}
	}


	public function delete($key)
	{
		$key = $this->key($key);
		$this->storage->delete($key);
	}


	protected function key($key)
	{
		return $this->group . '.' . $key;
	}
}